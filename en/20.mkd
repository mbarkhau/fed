Culmination at Jekyll Island
============================

Now that the groundwork had been laid for a central bank among scholars,
bankers, and interested public opinion, by the latter half of 1910 it
was time to formulate a concrete practical plan and to focus the rest of
the agitation to push it through. As Warburg wrote in the Academy of
Political Science book on Reform of the Currency: "Advance is possible
only by outlining a tangible plan" to set the terms of the debate.

The tangible plan phase of the central bank movement was launched by the
ever-pliant Academy of Political Science of Columbia University, which
held a monetary conference in November, 1910, in conjunction with the
New York Chamber of Commerce and the Merchants' Association of New York.
The members of the NMC were the joint guests of honor at this conclave,
and delegates to it were chosen by governors of twenty-two states, as
well as presidents of twenty-four chambers of commerce. Also attending
this conference were a large number of economists, monetary analysts and
representatives of the nation's leading bankers. Attendants at the
conference included Frank Vanderlip, Elihu Root, Jacob Schiff, Thomas W.
Lamont, partner of the Morgan bank, and J. P. Morgan himself. The formal
sessions of the conference were organized around papers delivered by
Laughlin, Johnson, Bush, Warburg, and Conant. C. Stuart Patterson, Dean
of the University of Pennsylvania Law School and member of the finance
committee of the Morgan-oriented Pennsylvania Railroad, who had been the
chairman of the first IMC and a member of the Indianapolis Monetary
Commission, laid down the marching orders for the assembled troops. He
recalled the great lesson of the IMC, and the way its proposals had
triumphed because "we went home and organized an aggressive and active
movement." He then exhorted the troops: "That is just what you must do
in this case, you must uphold the hands of Senator Aldrich. You have got
to see that the bill which he formulates . . . obtains the support of
every part of this country."

With the movement fully primed, it was now time for Senator Aldrich to
write the bill. Or rather, it was time for the senator, surrounded by a
few of the topmost leaders of the financial elite, to go off in
seclusion, and hammer out a detailed plan around which all parts of the
central banking movement could rally. Someone, probably Henry P.
Davison, got the idea of convening a small group of top leaders in a
super-secret conclave, to draft the bill. The eager J. P. Morgan
arranged for a plush private conference at his exclusive millionaire's
retreat, at the Jekyll Island Club on Jekyll Island, Georgia. Morgan was
a co-owner of the club. On November 22, 1910, Senator Aldrich, with a
handful of companions, set forth under assumed names in a privately
chartered railroad car from Hoboken, New Jersey to the coast of Georgia,
allegedly on a duck-hunting expedition.

The conferees worked for a solid week at the plush Jekyll Island
retreat, and hammered out the draft of the bill for the Federal Reserve
System. Only six people attended this super-secret week-long meeting,
and these six neatly reflected the power structure within the bankers'
alliance of the central banking movement. The conferees were, in
addition to Aldrich (Rockefeller kinsman); Henry P. Davison, Morgan
partner; Paul Warburg, Kuhn Loeb partner; Frank A. Vanderlip,
vice-president of Rockefeller's National City Bank of New York; Charles
D. Norton, president of Morgan's First National Bank of New York; and
Professor A. Piatt Andrew, head of the NMC research staff, who had
recently been made an Assistant Secretary of the Treasury under Taft,
and who was a technician with a foot in both the Rockefeller and Morgan
camps.

The conferees forged the Aldrich Bill, which, with only minor
variations, was to become the Federal Reserve Act of 1913. The only
substantial disagreement at Jekyll Island was tactical: Aldrich
attempted to hold out for a straightforward central bank on the European
model, while Warburg, backed by the other bankers, insisted that
political realities required the reality of central control to be
cloaked in the palatable camouflage of "decentralization." Warburg's
more realistic, duplicitous tactic won the day.

Aldrich presented the Jekyll Island draft, with only minor revisions, to
the full NMC as the Aldrich Bill in January, 1911. Why then did it take
until December, 1913 for Congress to pass the Federal Reserve Act? The
hitch in the timing resulted from the Democratic capture of the House of
Representatives in the 1910 elections, and from the looming probability
that the Democrats would capture the White House in 1912. The reformers
had to regroup, drop the highly partisan name of Aldrich from the bill,
and recast it as a Democratic bill under Virginia's Representative
Carter Glass. But despite the delay and numerous drafts, the structure
of the Federal Reserve as passed overwhelmingly in December 1913 was
virtually the same as the bill that emerged from the secret Jekyll
Island meeting three years earlier. Successful agitation brought
bankers, the business community, and the general public rather easily
into line.

The top bankers were brought into camp at the outset; as early as
February, 1911, Aldrich organized a closed-door conference of
twenty-three leading bankers at Atlantic City. Not only did this
conference of bankers endorse the Aldrich Plan, but it was made clear to
them that "the real purpose of the conference was to discuss winning the
banking community over to government control directly by the bankers for
their own ends." The big bankers at the conference also realized that
the Aldrich Plan would "increase the power of the big national banks to
compete with the rapidly growing state banks, (and) help bring the state
banks under control."[^33]

[^33]: Kolko, *Triumph of Conservatism,* p. 186.

By November, 1911, it was easy to line up the full American Bankers
Association behind the Aldrich Plan. The threat of small bank insurgency
was over, and the nation's banking community was now lined up solidly
behind the drive for a central bank. Finally, after much backing and
filling, after Aldrich's name was removed from the bill and Aldrich
himself decided not to run for reelection in 1912, the Federal Reserve
Act was passed overwhelmingly on December 22, 1913, to go into effect in
November of the following year. As A. Barton Hepburn exulted to the
annual meeting of the American Bankers Association in late August 1913:
"The measure recognizes and adopts the principles of a central bank.
Indeed, if it works out as the sponsors of the law hope, it will make
all incorporated banks together joint owners of a central dominating
power."[^34]

[^34]: Ibid., p. 235.
