Putting a Central Bank Across: Manipulating a Movement, 1897–1902
=================================================================

Around 1900, two mighty financial-industrial groups, each consisting of
investment banks, commercial banks, and industrial resources, confronted
each other, usually with hostility, in the financial, and more
importantly, the political arena. These coalitions were (1) the
interests grouped around the Morgan bank; and (2) an alliance of
Rockefeller–Harriman and Kuhn, Loeb interests. It became far easier for
these financial elites to influence and control politicians and
political affairs after 1900 than before. For the "Third Party System,"
which had existed in America from 1856 to 1896, was comprised of
political parties, each of which was highly ideological and in intense
conflict with the opposing party. While each political party, in this
case the Democratic, the Republican, and various minor parties,
consisted of a coalition of interests and forces, each was dominated by
a firm ideology to which it was strongly committed. As a result,
citizens often felt lifelong party loyalties, were socialized into a
party when growing up, were educated in party principles, and then rode
herd on any party candidates who waffled or betrayed the cause. For
various reasons, the Democratic and Republican parties after 1900, in
the Fourth and later Party Systems, were largely non-ideological,
differed very little from each other, and as a result commanded little
party loyalty. In particular, the Democratic Party no longer existed,
after the Bryan takeover of 1896, as a committed laissez-faire,
hard-money party. From then on, both parties rapidly became Progressive
and moderately statist.[^20]

[^20]: See, among others, Paul Kleppner, *The Third Electoral System*,
    1853–1892: *Parties, Voters, and Political Cultures* (Chapel Hill:
    University of North Carolina Press, 1979).

Since the importance of political parties dwindled after 1900, and
ideological laissez-faire restraints on government intervention were
gravely weakened, the power of financiers in government increased
markedly. Furthermore, Congress–the arena of political parties–became
less important. A power vacuum developed for the intellectuals and
technocratic experts to fill the executive bureaucracy, and to run and
plan national economic life relatively unchecked.

The House of Morgan had begun, in the 1860s and 70s, as an investment
bank financing and controlling railroads, and then, in later decades,
moved into manufacturing and commercial banking. In the opposing
coalition, the Rockefellers had begun in oil and moved into commercial
banking; Harriman had earned his spurs as a brilliant railroad investor
and entrepreneur in competition with the Morgans; and Kuhn, Loeb began
in investment banking financing manufacturing. From the 1890s until
World War II, much of American political history, of programs and
conflicts, can be interpreted not so much as "Democrat" versus
"Republican," but as the interaction or conflict between the Morgans and
their allies on the one hand, and the Rockefeller–Harriman-Kuhn, Loeb
alliance on the other.

Thus, Grover Cleveland spent his working life allied with the Morgans,
and his cabinet and policies were heavily Morgan-oriented; William
McKinley, on the other hand, a Republican from Rockefeller's home state
of Ohio, was completely in the Rockefeller camp. In contrast, McKinley's
vice-president, who suddenly assumed the presidency when McKinley was
assassinated, was Theodore Roosevelt, whose entire life was spent in the
Morgan ambit. When Roosevelt suddenly trotted out the Sherman Antitrust
Act, previously a dead letter, to try to destroy Rockefeller's Standard
Oil as well as Harriman's control of the Northern Pacific Railroad, this
led to a titanic struggle between the two mighty financial groups.
President Taft, an Ohio Republican who was close to the Rockefellers,
struck back by trying to destroy the two main Morgan trusts, United
States Steel and International Harvester. Infuriated, the Morgans
created the new Progressive Party in 1912, headed by Morgan partner
George W. Perkins, and induced the popular ex-President Roosevelt to run
for a third term on the Progressive ticket. The aim, and the result, was
to destroy Taft's chances for re-election, and to elect the first
Democratic president in twenty years, Woodrow Wilson.[^21]

[^21]: Thus, see Philip H. Burch, Jr., *Elites in American History,
    Vol. 2: From the Civil War to the New Deal* (New York: Holmes 
    & Meier, 1981).

But while the two financial groups clashed on many issues and
personalities, on some matters they agreed and were able to work in
concert. Thus, both groups favored the new trend toward cartelization in
the name of Progressivism and curbing alleged Big Business monopoly, and
both groups, led by the Morgans, were happy to collaborate in the
National Civic Federation.

On banking and on the alleged necessity for a central bank, both groups,
again, were in happy agreement. And while later on in the history of the
Federal Reserve there would be a mighty struggle for control between the
factions, to found the Fed they were able to work in undisturbed
harmony, and even tacitly agreed that the Morgan Bank would take the
lead and play the role of first among equals.[^22]

[^22]: J. P. Morgan's fondness for a central bank was heightened by the
    memory of the fact that the bank of which his father Junius was
    junior partner–the London firm of George Peabody and Company–was
    saved from bankruptcy in the Panic of 1857 by an emergency credit
    from the Bank of England. The elder Morgan took over the firm upon
    Peabody's retirement, and its name was changed to J. S. Morgan and
    Company. See Ron Chernow, *The House of Morgan* (New York: Atlantic
    Monthly Press, 1990), pp. 11–12.

The bank reform movement, sponsored by the Morgan and Rockefeller
forces, began as soon as the election of McKinley as President in 1896
was secured, and the populist Bryan menace beaten back. The reformers
decided not to shock people by calling for a central bank right away,
but to move toward it slowly, first raising the general point that the
money supply must be cured of its "inelasticity," The bankers decided to
employ the techniques they had used successfully in generating a mass
pro-gold standard movement in 1895 and 1896. The crucial point was to
avoid the damaging appearance of Wall Street prominence and control in
the new movement, by creating a spurious "grass roots" movement of
non-banker businessmen, centered in the noble American heartland of the
Middle West, far from the sinful environs of Wall Street. It was
important for bankers, *a fortiori* Wall Street bankers, to take a
discreet back seat in the reform movement, which was to consist
seemingly of heartland businessmen, academics, and other supposedly
disinterested experts.

The reform movement was officially launched just after the 1896 election
by Hugh Henry Hanna, president of the Atlas Engine Works of
Indianapolis, who had been active in the gold standard movement earlier
in the year; Hanna sent a memorandum to the Indianapolis Board of Trade
urging a heartland state like Indiana to take the lead in currency
reform.[^23] The reformers responded with remarkable speed. Answering
the call of the Indianapolis Board of Trade, delegates of Boards of
Trade from twelve midwestern cities met in Indianapolis at the beginning
of December, and they called for a large monetary convention of
businessmen from 26 states, which met quickly in Indianapolis on January
12. This Indianapolis Monetary Convention (IMC) resolved: (a) to urge
President McKinley to continue the gold standard; and (b) to urge the
president to create a new system of "elastic" bank credit, by appointing
a Monetary Commission to prepare legislation for a revised monetary
system. The IMC named Hugh Hanna as chairman of a permanent executive
committee that he would appoint to carry out these policies.

[^23]: For the memorandum, see by far the best book on the movement
    culminating in the Federal Reserve System, James Livingston, 
    *Origins of the Federal Reserve System: Money, Class, and Corporate
    Capitalism, 1890–1913* (Ithaca, N.Y.: Cornell University Press,
    1986).

The influential *Yale Review* hailed the IMC for deflecting opposition
by putting itself forward as a gathering of businessmen rather than
bankers. But to those in the know, it was clear that the leading members
of the executive committee were important financiers in the Morgan
ambit. Two particularly powerful executive members were Alexander E.
Orr, Morgan-oriented New York City banker, grain merchandiser, railroad
director, and director of the J. P. Morgan-owned publishing house of
Harper Brothers; and Milwaukee tycoon Henry C. Payne, a Republican
leader, head of the Morgan-dominated Wisconsin Telephone Company, and
long-time director of the North American Company, a giant public utility
holding company. So close was North American to the Morgan interests
that its board included two top Morgan financiers; Edmund C. Converse,
president of the Morgan-run Liberty National Bank of New York, and soon
to be founding president of Morgan's Bankers Trust Company; and Robert
Bacon, a partner in J. P. Morgan & Company, and one of Theodore
Roosevelt's closest friends.[^24]

[^24]: When Theodore Roosevelt became president he made Bacon
    Assistant Secretary of State, while Henry Payne took the top
    political job of Postmaster General of the United States.

A third member of the IMC executive committee was an even more powerful
secretary of the committee and was even closer to the Morgan empire. He
was George Hoster Peabody. The entire Peabody family of Boston Brahmins
had long been personally and financially closely associated with the
Morgans. A George Peabody had established an international banking firm
of which J. P. Morgan's father, Junius, had been a senior partner. A
member of the Peabody clan had served as best man at J. P. Morgan's
wedding in 1865. George Foster Peabody was an eminent, politically
left-liberal, New York investment banker, who was to help the Morgans
reorganize one of their prime industrial firms, General Electric, and
who was later offered the job of Secretary of Treasury in the Wilson
Administration. Although he turned down the official post, Peabody
functioned throughout the Wilson regime as a close adviser and
"statesman without portfolio."

President McKinley was highly favorable to the IMC, and in his First
Inaugural Address, he endorsed the idea of "some revision" of the
banking system. He followed this up in late July, 1897 with a special
message to Congress, proposing the establishment of a special monetary
commission. A bill for a commission passed the House, but failed in the
Senate.

Disappointed but imperturbable, the executive committee of the IMC
decided in August to select their own Indianapolis Monetary Commission.
The leading role in appointing the new Commission was played by George
Foster Peabody. The Commission consisted of various industrial notables,
almost all either connected with Morgan railroads or, in one case, with
the Morgan-controlled General Electric Company.[^25] The working head
of the Monetary Commission was economist J. Laurence Laughlin, head
Professor of Political Economy at the University of Chicago, and editor
of the university's prestigious *Journal of Political Economy*.
Laughlin supervised the operations of the Commission staff and the
writings of its reports; the staff consisted of two of Laughlin's
graduate students at Chicago.

[^25]: Some examples: Former Secretary of the Treasury (under Cleveland)
    Charles S. Fairchild, a leading New York banker, former partner in
    the Boston Brahmin, Morgan-oriented investment banking firm of Lee,
    Higginson & Company. Fairchild's father, Sidney T., had been a
    leading attorney for the Morgan-controlled New York Central
    Railroad. Another member of the Commission was Stuyvesant Fish, 
    scion of two long-time aristocratic New York families, partner of
    the Morgan-dominated New York investment bank of Morton, Bliss & 
    Company, and president of the Illinois Central Railroad. A third
    member was William B. Dean, merchant from St. Paul, Minnesota, and
    a director of the St. Paul-based transcontinental railroad, Great
    Northern, owned by James J. Hill, a powerful ally of Morgan in his
    titanic battle with Harriman, Rockefeller, and Kuhn, Loeb for
    control of the Northern Pacific Railroad.

The then impressive sum of $50,000 was raised throughout the nation's
banking and corporate community to finance the work of the Indianapolis
Monetary Commission. New York City's large quota was raised by Morgan
bankers Peabody and Orr, and a large contribution came from none other
than J. P. Morgan himself.

Setting up shop in Washington in mid-September, the Commission staff
pioneered in persuasive public relations techniques to spread the
reports of the Commission far and wide. In the first place, they sent a
detailed monetary questionnaire to several hundred selected "impartial"
experts, who they were sure would answer the questions in the desired
manner. These questionnaire answers were then trumpeted as the received
opinions of the nation's business community. Chairman of the IMC Hugh
Hanna made the inspired choice of hiring as Washington assistant of the
Commission the financial journalist Charles A. Conant, who had recently
written *A History of Modern Banks of Issue*. The Monetary Commission
was due to issue its preliminary report in mid-December; by early
December, Conant was beating the drums for the Commission's
recommendations, leading an advance line of the report in an issue of
*Sound Currency* magazine, and bolstering the Commission's proposals
with frequent reports of unpublished replies to the Commission's
questionnaire. Conant and his colleagues induced newspapers throughout
the country to print abstracts of these questionnaire answers, and in
that way, as the Commission's secretary reported, by "careful
manipulation" were able to get part or all of the preliminary Commission
report printed in nearly 7,500 newspapers, large and small, across the
nation. Thus, long before the days of computerized direct mail, Conant
and the others on the staff developed a distribution or transmission
system of nearly 100,000 correspondents "dedicated to the enactment of
the commission's plan for banking and currency reform."[^26]

[^26]: Livingston, *Origins,* pp. 109\u201310.

The prime emphasis of the Commission's preliminary report was to
complete the McKinley victory by codifying the existing *de facto*
single gold standard. More important in the long run was a call for
fundamental banking reform to allow greater "elasticity," so that bank
credit could be increased during recessions. As yet, there were little
specifics for such a long-run transformation.

The executive committee now decided to organize the second and final
meeting of the Indianapolis Monetary Convention, which met at that city
in January, 1898. The second convention was a far grander affair than
the first, bringing together nearly 500 delegates from 31 states.
Moreover, the gathering was a cross-section of America's top corporate
leaders. The purpose of this second convention, as former Secretary of
the Treasury Fairchild candidly explained to the gathering, was to
mobilize the nation's leading businessmen into a mighty and influential
banking reform movement. As he put it, "If men of business give serious
attention and study to these subjects, they will substantially agree
upon legislation, and thus, agreeing, their influence will be
prevailing." Presiding officer of the convention, Iowa's Governor Leslie
M. Shaw, was, however, a bit disingenuous when he told the meeting: "You
represent today not the banks, for there are few bankers on this floor.
You represent the business industries and the financial interests of the
country." For there were plenty of bankers there, too. Shaw himself,
later to be Secretary of the Treasury under Theodore Roosevelt, was a
small-town banker in Iowa, president of the Bank of Denison, who saw
nothing wrong with continuing in this post throughout his term as
governor. More important for Shaw's career was the fact that he was a
long-time leading member of the Des Moines Regency, the Iowa Republican
machine headed by powerful Senator William Boyd Allison. Allison, who
was later to obtain the Treasury post for Shaw, was in turn closely tied
to Charles E. Perkins, a close Morgan ally, president of the Chicago,
Burlington and Quincy Railroad, and kinsman of the highly influential
Forbes financial group of Boston, long tied to the Morgan interests.

Also serving as delegates to this convention were several eminent
economists who, however, intriguingly came not as academic observers but
frankly as representatives of sections of the business community. Thus
Professor Jeremiah W. Jenks of Cornell, a leading proponent of
government cartelization and enforcement of trusts and soon to become a
friend and advisor of Theodore Roosevelt as governor of New York, came
as a delegate from the Ithaca Business Men's Association. Frank W.
Taussig of Harvard represented the Cambridge Merchant's Association;
Yale's Arthur Twining Hadley, soon to become president of Yale
University, came as representative of the New Haven Chamber of Commerce;
and Fred M. Taylor of the University of Michigan came representing the
Ann Arbor Business Men's Association. Each of these men held powerful
posts in the organized economics profession, Jenks, Taussig, and Taylor
serving on the Currency Committee of the American Economic Association.
Hadley, a leading railroad economist, also served on the board of
directors of two leading Morgan railroads: the New York, New Haven and
Hartford, and the Atchison, Topeka, and Santa Fe Railroads.

Both Taussig and Taylor were monetary theorists who urged reform to make
the money supply more elastic. Taussig wanted an expansion of national
bank notes, to inflate in response to the "needs of business," so that
the currency would "grow without trammels as the needs of the community
spontaneously call for increase." Taylor, too, urged a modification of
the gold standard by "a conscious control of the movement of money" by
government "in order to maintain the stability of the credit system."
Taylor went so far as to justify suspensions of specie payment by the
government in order to "protect the gold reserve."[^27]

[^27]: Joseph Dorfman, *The Economic Mind in American Civilization* 
    York: Viking Press, 1949), vol. 3, pp. xxxviii, 269, 392–93.

In late January, the Convention duly endorsed the preliminary report
with virtual unanimity, after which Professor Laughlin was assigned the
task of drawing up a more elaborate Final Report of the Commission,
which was published and distributed a few months later. With the
endorsement of the august membership of the Convention secured,
Laughlin's Final Report finally let the cat out of the bag: for the
report not only came out for a greatly increased issue of national bank
notes, but it also called explicitly for a Central Bank that would enjoy
a monopoly of the issue of bank notes.[^28]

[^28]: The Final Report, including the recommendation for a
    Central Bank, was hailed by Convention delegate F. M. Taylor in
    Laughlin's economic journal, the *Journal of Political Economy*.
    Taylor also exulted that Convention had been "one of the most
    notable movements of our time–the first thoroughly organized
    movement of the business classes in the whole country directed to
    the bringing about of a radical change in national legislation."
    F. M.  Taylor, "The Final Report to the Indianapolis Monetary
    Commission," *Journal of Political Economy* 6 (June 1898): 322.

The Convention delegates promptly took the gospel of banking reform and
a central bank to the length and breadth of the corporate and financial
communities. Thus, in April, 1898, A. Barton Hepburn, monetary historian
and president of the Chase National Bank of New York, at that time the
flagship commercial bank for the Morgan interests, and a man who would
play a leading role in the drive to establish a central bank, invited
Monetary Commissioner Robert S. Taylor to address the New York State
Bankers' Association on the currency question, since "bankers, like
other people, need instruction on this subject." All the Monetary
Commissioners, especially Taylor, were active during this period in
exhorting groups of businessmen throughout the nation on behalf of
banking reform.[^29]

[^29]: Taylor was an Indiana attorney for General Electric Company.

Meanwhile, the lobbying team of Hanna and Conant were extremely active
in Washington. A bill embodying the proposals of the Indianapolis
Monetary Commission was introduced into the House by Indiana Congressman
Jesse Overstreet in January, and was reported out by the House Banking
and Currency Committee in May. In the meanwhile, Conant met also
continually with the Banking Committee members, while Hanna repeatedly
sent circular letters to the Convention delegates and to the public,
urging a letter-writing campaign in support of the bill at every step of
the Congressional process.

Amidst this agitation, McKinley's Secretary of the Treasury, Lyman J.
Gage, worked closely with Hanna, Conant, and their staff. Gage sponsored
several bills along the same lines. Gage, a friend of several of the
Monetary Commissioners, was one of the top leaders of the Rockefeller
interests in the banking field. His appointment as Secretary of the
Treasury had been secured for him by Ohio's Mark Hanna, political
mastermind and financial backer of President McKinley, and old friend,
high school classmate, and business associate of John D. Rockefeller,
Sr. Before his appointment to the Cabinet, Gage had been president of
the powerful First National Bank of Chicago, one of the leading
commercial banks in the Rockefeller ambit. During his term in office,
Gage tried to operate the Treasury Department as a central bank, pumping
in money during recessions by purchasing government bonds in the open
market, and depositing large funds with pet commercial banks.

In 1900, Gage called vainly for the establishment of regional central
banks. Finally, in his last annual report as Secretary of the Treasury
in 1901, Lyman Gage called outright for a governmental central bank.
Without such a central bank, he declared in alarm, "individual banks
stand isolated and apart, separated units, with no tie of mutuality
between them." Unless a central bank could establish such ties, he
warned, the Panic of 1893 would be repeated.

Any reform legislation, however, had to wait until the gold forces could
secure control of Congress in the elections of 1898. In the autumn, the
permanent executive committee of the Indianapolis Monetary Convention
mobilized its forces, calling on 97,000 correspondents throughout the
country to whom it had distributed its preliminary report. The executive
committee urged its readers to elect a gold standard Congress, a task
which was accomplished in November.

As a result, the McKinley Administration now could submit its bill to
codify the single gold standard, which Congress passed as the Gold
Standard Act of March, 1900. Phase One of the reformers' task had been
accomplished: gold was the sole standard, and the silver menace had been
crushed. Less well-known are the clauses of the Gold Standard Act that
began the march toward a more "elastic" currency. Capital requirements
for national banks in small towns and rural areas were now loosened, and
it was made easier for national banks to issue notes. The purpose was to
meet a popular demand for "more money" in rural areas at crop-moving
time.

But the reformers regarded the Gold Standard Act as only the first step
toward fundamental banking reform. Thus, Frank Taussig, in Harvard's
economic journal, praised the Gold Standard Act, and was particularly
gratified that it was the result of a new social and ideological
alignment, sparked by "strong pressure from the business community"
through the Indianapolis Monetary Convention. But Taussig warned that
more reform was needed to allow for greater expansion of money and bank
credit.[^30]

[^30]: Frank W. Taussig, "The Currency Act of 1900," *Quarterly Journal
    of Economics* 14 (May 1900): 415.

More detailed in calling for reform in his comment on the Gold Standard
Act was Joseph French Johnson, Professor of Finance at the Wharton
School of Business at the University of Pennsylvania. Johnson deplored
the U. S. banking system as the worst in the world, and pointed in
contrast to the glorious central banking systems existing in Britain and
France. In the United States, however, unfortunately "there is no single
business institution, and no group of large institutions, in which
self-interest, responsibility, and power naturally unite and conspire
for the protection of the monetary system against twists and strains."
In short, there was far too much freedom and decentralization in the
banking system, so that the deposit credit structure "trembles" whenever
credit expansion leads to demands for cash or gold.[^31] Johnson had
been a mentor and close friend at the *Chicago Tribune* of both Lyman
Gage and Frank A. Vanderlip, who was to play a particularly
important role in the drive for a central bank. When Gage went to
Washington as Secretary of the Treasury, he brought Vanderlip along as
his Assistant Secretary. On the accession of Roosevelt to the
Presidency, Gage left the Cabinet in early 1902, and Gage, Vanderlip,
and Conant all left for top banking positions in New York.[^32]

[^31]: Joseph French Johnson, "The Currency Act of March 14, 1900,"
    *Political Science Quarterly* 15 (1900): 482–507.

[^32]: Gage became president of the Rockefeller-controlled U.S. 
    Trust Company; Vanderlip became vice-president at the flagship
    commercial bank of the Rockefeller interests, the National City 
    Bank of New York; and Conant became Treasurer of the 
    Morgan-controlled Morton Trust Company.

The political pressure for reform after 1900 was poured on by the large
bankers. A. Barton Hepburn, head of Morgan's Chase National Bank, drew
up a bill as head of a commission of the American Bankers Association,
and the bill was submitted to Congress in late 1901 by Representative
Charles N. Fowler of New Jersey, chairman of the House Banking and
Currency Committee. The Hepburn–Fowler Bill was reported out of the
committee the following April. The Fowler Bill allowed for further
expansion of national bank notes; it also allowed national banks to
establish branches at home and abroad, a step that had been illegal (and
has still been illegal until very recently) due to the fierce opposition
of the small country bankers. Third, the Fowler Bill proposed to create
a three-member board of control within the Treasury Department to
supervise new bank notes and to establish clearinghouses. This would
have been a step toward a central bank. But, at this point, fierce
opposition by the country bankers managed to kill the Fowler Bill on the
floor of the House in 1902, despite agitation in its favor by the
executive committee and staff of the Indianapolis Monetary Convention.

Thus, the opposition of the small country bankers managed to stop the
reform drive in Congress. Trying another tack, Theodore Roosevelt's
Secretary of Treasury Leslie Shaw attempted to continue and expand Lyman
Gage's experiments in making the U.S. Treasury function like a central
bank. In particular, Shaw made open-market purchases in recessions and
violated the Independent Treasury statutes confining Treasury funds to
its own vaults, by depositing Treasury funds in favored large national
banks. In his last annual report of 1906, Secretary Shaw urged that he
be given total power to regulate all the nation's banks. But by this
time, the reformers had all dubbed these efforts a failure; a central
bank itself was deemed clearly necessary.
