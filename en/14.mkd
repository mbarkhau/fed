The Central Bank Buys Assets
============================

Before analyzing operations of the Federal Reserve in more detail, we
should understand that the most important way that a Central Bank can
cartelize its banking system is by increasing the reserves of the banks,
and the most important way to do that is simply by buying assets.

In a gold standard, the "reserve" of a commercial bank, the asset that
allegedly stands behind its notes or deposits, is gold. When the Central
Bank enters the scene, and particularly after the Peel Act of 1844, the
reserves consist of gold, but predominantly they consist of the bank's
demand deposit account at the Central Bank, an account which enables the
bank to redeem its own checking account in the notes of the Central
Bank, which enjoys a State-granted monopoly on the issue of tangible
notes. As a result, in practice the banks hold Central Bank deposits as
their reserve and they redeem in Central Bank notes, whereas the Central
Bank is pledged to redeem those notes in gold.

This post-Peel Act structure, it is clear, not undesignedly paved the
way for a smooth transition to a fiat paper standard. Since the average
citizen had come to use Central Bank notes as his cash, and gold was
demanded only by foreigners, it was relatively easy and not troublesome
for the government to go off gold and to refuse to redeem its or its
Central Bank notes in specie. The average citizen continued to use Bank
notes and the commercial banks continued to redeem their deposits in
those notes. The daily economic life of the country seemed to go on much
as before. It should be clear that, if there had been no Central Bank,
and especially no Central Bank with a Peel Act type monopoly of notes,
going off gold would have created a considerable amount of trouble and a
public outcry.

How, then, can the Central Bank increase the reserves of the banks under
its jurisdiction? Simply by buying assets. It doesn't matter *whom* it
buys assets from, whether from the banks or from any other individual or
firm in the economy. Suppose a Central Bank buys an asset from a bank.
For example, the Central Bank buys a building, owned by the Jonesville
Bank for $1,000,000. The building, appraised at $1,000,000, is
transferred from the asset column of the Jonesville Bank to the asset
column of the Central Bank. How does the Central Bank pay for the
building? Simple: by writing out a check on itself for $1,000,000. Where
did it get the money to write out the check? It created the money out of
thin air, i.e., by creating a fake warehouse receipt for $1,000,000 in
cash which it does not possess. The Jonesville Bank deposits the check
at the Central Bank, and the Jonesville Bank's deposit account at the
Central Bank goes up by $1,000,000. The Jonesville Bank's total reserves
have increased by $1,000,000, upon which it and other banks will be
able, in a short period of time, to multiply their own warehouse
receipts to non-existent reserves manyfold, and thereby to increase the
money supply of the country manyfold.

Figure 7 demonstrates this initial process of purchasing assets. We now
have to deal with two sets of T-accounts: the commercial bank and the
Central Bank The process is shown as in figure 7.

![](images/img08.jpg)

Now, let us analyze the similar, though less obvious, process that
occurs when the Central Bank buys an asset from anyone, any individual
or firm, in the economy. Suppose that the Central Bank buys a house
worth $1,000,000 from Jack Levitt, homebuilder. The Central Bank's asset
column increases by $1,000,000 for the house; again, it pays for the
house by writing a $1,000,000 check on itself, a warehouse receipt for
non-existent cash it creates out of thin air. It writes out the check to
Mr. Levitt. Levitt, who cannot have an account at the Central Bank (only
banks can do so), can do only one thing with the check: deposit it at
whatever bank he uses. This increases his checking account by
$1,000,000. Now, here there is a variant on the events of the previous
example. Already, in the one act of depositing this check, the total
money supply in the country has increased by $1,000,000, a $1,000,000
which did not exist before. So an inflationary increase in the money
supply has already occurred. And redistribution has already occurred as
well, since all of the new money, at least initially, resides in the
possession of Mr. Levitt. But this is only the initial increase, for the
bank used by Levitt, say the Rockville Bank, takes the check and
deposits it at the Central Bank, thereby gaining $1,000,000 in its
deposits, which serve as its reserves for its own fractional-reserve
banking operations. The Rockville Bank, accompanied by other, competing
banks, will then be able to pyramid an expansion of multiple amounts of
warehouse receipts and credits, which will comprise the new warehouse
receipts being loaned out. There will be a multiple expansion of the
money supply. This process can be seen in Figures 8 and 9 below.

![](images/img09.jpg)

At this point, the commercial bank has an increase in its reserves–its
demand deposits at the Central Bank–of $1,000,000. This bank,
accompanied by its fellow commercial banks, can now expand a multiple of
loans and demand deposits on top of those reserves. Let us assume–a
fairly realistic assumption–that that multiple is 10-to-l. The bank
feels that now it can expand its demand deposits to 10 times its
reserves. It now creates new demand deposits in the process of lending
them out to businesses or consumers, either directly or in the course of
purchasing securities on the market. At the end of this expansion
process taking a few weeks, the bank's balance sheet can be seen in
Figure 9 below. Note that the situation in Figure 9 could have resulted,
either from the direct purchase of an asset by the Central Bank from the
commercial bank itself (Figure 7), *or* by purchasing an asset in the
open market from someone who is a depositor at this or another
commercial bank (Figure 8). The end result will be the same.

![](images/img10.jpg)

