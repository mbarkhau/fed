The Fed At Last: Morgan-Controlled Inflation
============================================

The new Federal Reserve System had finally brought a central bank to
America: the push of the big bankers had at last succeeded. Following
the crucial plank of post-Peel Act Central Banking, the Fed was given a
monopoly of the issue of all bank notes; national banks, as well as
state banks, could now only issue deposits, and the deposits had to be
redeemable in Federal Reserve Notes as well as, at least nominally, in
gold. All national banks were "forced" to become members of the Federal
Reserve System, a "coercion" they had long eagerly sought, which meant
that national bank reserves had to be kept in the form of demand
deposits, or checking accounts, at the Fed. The Fed was now in place as
lender of last resort; and with the prestige, power, and resources of
the U. S. Treasury solidly behind it, it could inflate more consistently
than the Wall Street banks under the National Banking System, and above
all, it could and did, inflate even during recessions, in order to bail
out the banks. The Fed could now try to keep the economy from recessions
that liquidated the unsound investments of the inflationary boom, and it
could try to keep the inflation going indefinitely.

At this point, there was no need for even national banks to hold onto
gold; they could, and did, deposit their gold into the vaults of the
Fed, and receive reserves upon which they could pyramid and expand the
supply of money and credit in a coordinated, nation-wide fashion.
Moreover, with reserves now centralized into the vaults of the Fed, bank
reserves could be, as the bank apologists proclaimed, "economized,"
i.e., there could be and was more inflationary credit, more bank
"counterfeiting," pyramided on top of the given gold reserves. There
were now three inverted inflationary pyramids of bank credit in the
American economy: the Fed pyramided *its* notes and deposits on top of
its newly centralized gold supply; the national banks pyramided bank
deposits on top of their reserves of deposits at the Fed; and those
state banks who chose not to exercise their option of joining the
Federal Reserve System could keep their deposit accounts at national
banks and pyramid *their* credit on top of that. And at the base of
the pyramid, the Fed could coordinate and control the inflation by
determining the amount of reserves in the member banks.

To give an extra fillip to monetary inflation, the new Federal Reserve
System cut in half the average legal minimum reserve requirements
imposed on the old national banks. Whereas the national banks before the
onset of the Fed were required to keep an average minimum of 20 percent
reserves to demand deposits, on which they could therefore pyramid
inflationary money and credit of 5:1, the new Fed halved the minimum
reserve requirement on the banks to 10 percent, doubling the
inflationary bank pyramiding in the country to 10:1.

As luck would have it, the new Federal Reserve System coincided with the
outbreak of World War I in Europe, and it is generally agreed that it
was only the new system that permitted the U.S. to enter the war and to
finance both its own war effort, and massive loans to the allies;
roughly, the Fed doubled the money supply of the U.S. during the war and
prices doubled in consequence. For those who believe that U.S. entry
into World War I was one of the most disastrous events for the U.S. and
for Europe in the twentieth century, the facilitating of U.S. entry into
the war is scarcely a major point in favor of the Federal Reserve.

In form as well as in content, the Federal Reserve System is precisely
the cozy government-big bank partnership, the government-enforced
banking cartel, that big bankers had long envisioned. Many critics of
the Fed like to harp on the fact that the private bankers legally own
the Federal Reserve System, but this is an unimportant legalistic fact;
Fed (and therefore possible bank) profits from its operations are taxed
away by the Treasury. The benefits to the bankers from the Fed come not
from its legal profits but from the very essence of its operations: its
task of coordination and backing for bank credit inflation. *These*
benefits dwarf any possible direct profits from the Fed's banking
operations into insignificance.

From the beginning, the Fed has been headed by a Federal Reserve Board
in Washington, all appointed by the President with the consent of the
Senate. The Board supervises the twelve "decentralized" regional Federal
Reserve Banks, whose officers are indeed selected by private banks in
each region, officers who have to be approved by the Washington Board.

At the outset of the Fed, and until the "reforms" of the 1930s, the
major control of the Fed was not in the hands of the Board, but of the
Governor (now called "President") of the Federal Reserve Bank of New
York, Wall Street's main man in the Fed System.[^35] The major
instrument of Fed control of the money and banking system is its "open
market operations": its buying and selling of U.S. government securities
(or, indeed, any other asset it wished) on the open market. (We will see
how this process works below.) Since the U.S. bond market is located in
Wall Street, the Governor of the New York Fed was originally in almost
sole control of the Fed's open market purchases and sales, and hence of
the Federal Reserve itself. Since the 1930s, however, the crucial open
market policies of the Fed have been decided by a Federal Open Market
Committee, which meets in Washington, and which includes all the seven
members of the Board of Governors plus a rotating five of the twelve
largely banker-selected Presidents of the regional Feds.

[^35]: Because of the peculiarities of banking history, "Governor" is
    considered a far more exalted title than "President," a status 
    stemming from the august title of "Governor" as head of the original
    and most prestigious Central Bank, the Bank of England. Part of the
    downgrading of the regional Federal Reserve Banks and upgrading of
    power of the Washington Board in the 1930s was reflected in the change
    of the title of head of each regional Bank from "Governor" to
    "President," matched by the change of title of the Washington board
    from "Federal Reserve Board" to "Board of Governors of the Federal
    Reserve System."

There are two critical steps in the establishment and functioning of any
cartel-like government regulation. We cannot afford to ignore either
step. Step one is passing the bill and establishing the structure. The
second step is selecting the proper personnel to run the structure:
there is no point to big bankers setting up a cartel, for example, and
then see the personnel fall into the "wrong" hands. And yet conventional
historians, not geared to power elite or ruling elite analysis, usually
fall down on this crucial second task, of seeing precisely *who* the
new rulers of the system would be.

It is all too clear, on examining the origin and early years of the Fed,
that, both in its personnel and chosen monetary and financial policies,
the Morgan Empire was in almost supreme control of the Fed.

This Morgan dominance was not wholly reflected in the makeup of the
first Federal Reserve Board. Of the seven Board members, at the time two
members were automatically and *ex officio* the Secretary of the
Treasury and the Comptroller of the Currency, the regulator of the
national banks who is an official in the Treasury Department. The
Secretary of Treasury in the Wilson Administration was his son-in-law
William Gibbs McAdoo, who, as a failing businessman and railroad magnate
in New York City, had been personally befriended and bailed out by J. P.
Morgan and his close associates. McAdoo spent the rest of his financial
and political life in the Morgan ambit. The Comptroller of the Currency
was a long-time associate of McAdoo's, John Skelton Williams. Williams
was a Virginia banker, who had been a director of McAdoo's Morgan
controlled Hudson & Manhattan Railroad and president of the
Morgan-oriented Seaboard Airline Railway.

These Treasury officials on the Board were reliable Morgan men, but they
were members only *ex officio*. Governor (now "Chairman") of the
original board was Charles S. Hamlin, whom McAdoo had appointed as
Assistant Secretary of Treasury along with Williams. Hamlin was a Boston
attorney who had married into the wealthy Pruyn family of Albany, a
family long connected with the Morgan-dominated New York Central
Railroad. Another member of the Federal Reserve Board, and a man who
succeeded Hamlin as Governor, was William P. G. Harding, a protégé of
Alabama Senator Oscar W. Underwood, whose father-in-law, Joseph H.
Woodward, was vice-president of Harding's First National Bank of
Birmingham, Alabama, and head of the Woodward Iron Company, whose board
included representatives of both Morgan and Rockefeller interests. The
other three Board members were Paul M. Warburg; Frederic A. Delano,
uncle of Franklin D. Roosevelt and president of the
Rockefeller-controlled Wabash Railway; and Berkeley economics professor
Adolph C. Miller, who had married into the wealthy, Morgan-connected
Sprague family of Chicago.[^36]

[^36]: Miller's father-in-law, Otho S. A. Sprague, had served as a 
    director of the Morgan-dominated Pullman Company, while Otho's 
    brother Albert A. Sprague, was a director of the Chicago Telephone
    Company, a subsidiary of the mighty Morgan-controlled monopoly
    American Telephone & Telegraph Company.
    
    It should be noted that while the Oyster Bay-Manhattan branch of 
    the Roosevelt family (including President Theodore Roosevelt) had 
    long been in the Morgan ambit, the Hyde Park branch (which of 
    course included F.D.R.) was long affiliated with their wealthy and
    influential Hudson Valley neighbors, the Astors and the Harrimans.

Thus, if we ignore the two Morgan *ex-officios*, the Federal Reserve
Board in Washington began its existence with one reliable Morgan man,
two Rockefeller associates (Delano and a leader of close Rockefeller
ally, Kuhn, Loeb), and two men of uncertain affiliation: a prominent
Alabama banker, and an economist with vague family connections to Morgan
interests. While the makeup of the Board more or less mirrored the
financial power-structure that had been present at the Fed's critical
founding meeting at Jekyll Island, it could scarcely guarantee
unswerving Morgan control of the nation's banking system.

That control was guaranteed, instead, by the identity of the man who was
selected to the critical post of Governor of the New York Fed, a man,
furthermore, who was by temperament very well equipped to seize in fact
the power that the structure of the Fed could offer him. That man, who
ruled the Federal Reserve System with an iron hand from its inception
until his death in 1928, was one Benjamin Strong.[^37]

[^37]: On the personnel of the original Fed, see Murray N. Rothbard,
    "The Federal Reserve as a Cartelization Device: The Early Years, 
    1913–1939," in *Money in Crisis,* Barry Siegel, ed. (San 
    Francisco: Pacific Institute for Public Policy Research, 1984),
    pp. 94–115.

Benjamin Strong's entire life had been a virtual preparation for his
assumption of power at the Federal Reserve. Strong was a long-time
protege of the immensely powerful Henry P. Davison, number two partner
of the Morgan Bank just under J. P. Morgan himself, and effective
operating head of the Morgan World Empire. Strong was a neighbor of
Davison's in the then posh suburb of New York, Englewood, New Jersey,
where his three closest friends in the world became, in addition to
Davison, two other Morgan partners; Dwight Morrow and Davison's main
protege as Morgan partner, Thomas W. Lamont. When the Morgans created
the Bankers Trust Company in 1903 to compete in the rising new trust
business, Davison named Strong as its secretary, and by 1914 Strong had
married the firm's president's daughter and himself risen to president
of Bankers Trust. In addition, the Davisons raised Strong's children for
a time after the death of Strong's first wife; moreover, Strong served
J. P. Morgan as his personal auditor during the Panic of 1907.

Strong had long been a voluble advocate of the original Aldrich Plan,
and had participated in a lengthy August, 1911 meeting on the Plan with
the Senator Davison, Vanderlip and a few other bankers on Aldrich's
yacht. But Strong was bitterly disappointed at the final structure of
the Fed, since he wanted a "real central bank ... run from New York
by a board of directors on the ground"–that is, a Central Bank openly
and candidly run from New York and dominated by Wall Street. After a
weekend in the country, however, Davison and Warburg persuaded Strong to
change his mind and accept the proffered appointment as Governor of the
New York Fed. Presumably, Davison and Warburg convinced him that Strong,
as effective head of the Fed, could achieve the Wall Street-run banking
cartel of his dreams if not as candidly as he would have wished. As at
Jekyll Island, Warburg persuaded his fellow cartelist to bow to the
political realities and adopt the cloak of decentralization.

After Strong assumed the post of Governor of the New York Fed in
October, 1914, he lost no time in seizing power over the Federal Reserve
System. At the organizing meeting of the System, an extra-legal council
of regional Fed governors was formed; at its first meeting, Strong
grabbed control of the council, becoming both its chairman and the
chairman of its executive committee. Even after W. P. G. Harding became
Governor of the Federal Reserve Board two years later and dissolved the
council, Strong continued as the dominant force in the Fed, by virtue of
being named sole agent for the open-market operations of all the
regional Federal Reserve Banks. Strong's power was further entrenched by
U.S. entry into World War I. Before then, the Secretary of the Treasury
had continued the legally mandated practice since Jacksonian times of
depositing all government funds in its own sub-treasury branch vaults,
and in making all disbursements from those branches. Under spur of
wartime, however, McAdoo fulfilled Strong's long-standing ambition:
becoming the sole fiscal agent for the U.S. Treasury. From that point
on, the Treasury deposited its funds with the Federal Reserve.

Not only that: wartime measures accelerated the permanent nationalizing
of the gold stock of Americans, the centralization of gold into the
hands of the Federal Reserve. This centralization had a twofold effect:
it mobilized more bank reserves to spur greater and
nationally-coordinated inflation of bank credit; and it weaned the
average American from the habit of using gold in his daily life and got
him used to substituting paper or checking accounts instead. In the
first place, the Federal Reserve law was changed in 1917 to permit the
Fed to issue Federal Reserve Notes in exchange for gold; before that, it
could only exchange its notes for short-term commercial bills. And
second, from September, 1917 until June, 1919 the United States was *de
facto* off the gold standard, at least for gold redemption of dollars
to foreigners. Gold exports were prohibited and foreign exchange
transactions were controlled by the government. As a result of both
measures, the gold reserves of the Federal Reserve, which had
constituted 28 percent of the nation's gold stock on U.S. entry into the
war, had tripled by the end of the war to 74 percent of the country's
gold.[^38]

[^38]: On Benjamin Strong's seizure of supreme power in the Fed and its
    being aided by wartime measures, see Lawrence E. Clark, *Central 
    Banking Under the Federal Reserve System* (New York: Macmillan, 
    1935), pp. 64–102–5; Lester V. Chandler, *Benjamin Strong: Central
    Banker* (Washington, D.C.: Brookings Institution, 1958), pp. 23–41,
    68–78, 105–7; and Henry Parker Willis, *The Theory and Practice of
    Central Banking* (New York: Harper & Brothers, 1936), pp. 90–91.

The content of Benjamin Strong's monetary policies was what one might
expect from someone from the highest strata of Morgan power. As soon as
war broke out in Europe, Henry P. Davison sailed to England, and was
quickly able to use long-standing close Morgan ties with England to get
the House of Morgan named as sole purchasing agent in the United States,
for the duration of the war, for war material for Britain and France.
Furthermore, the Morgans also became the sole underwriter for all the
British and French bonds to be floated in the U.S. to pay for the
immense imports of arms and other goods from the United States. J. P.
Morgan and Company now had an enormous stake in the victory of Britain
and France, and the Morgans played a major and perhaps decisive role in
maneuvering the supposedly "neutral" United States into the war on the
British side.[^39]

[^39]: On the Morgans, their ties to the British, and their influence
    on America's entry into the war, see Charles Callan Tansill, 
    *America Goes to War* (Boston: Little, Brown, 1938).

The Morgan's ascendancy during World War I was matched by the relative
decline of the Kuhn, Loebs. The Kuhn, Loebs, along with other prominent
German-Jewish investment bankers on Wall Street, supported the German
side in the war, and certainly opposed American intervention on the
Anglo-French side. As a result, Paul Warburg was ousted from the Federal
Reserve Board, the very institution he had done so much to create. And
of all the leading "Anglo" financial interests, the Rockefellers, ally
of the Kuhn, Loebs, and a bitter rival of the Angle-Dutch Royal Dutch
Shell Oil Company for world oil markets and resources, was one of the
very few who remained unenthusiastic about America's entry into the
war.

During World War I, Strong promptly used his dominance over the banking
system to create a doubled money supply so as to finance the U. S. war
effort and to insure an Anglo-French victory. All this was only prelude
for a Morgan-installed monetary and financial policy throughout the
1920s. During the decade of the twenties, Strong collaborated closely
with the Governor of the Bank of England, Montagu Norman, to inflate
American money and credit so as to support the return of Britain to a
leading role in a new form of bowdlerized gold standard, with Britain
and other European countries fixing their currencies at a highly
over-valued par in relation to the dollar or to gold. The result was a
chronic export depression in Britain and a tendency for Britain to lose
gold, a tendency that the United States felt forced to combat by
inflating dollars in order to stop the hemorrhaging of gold from Great
Britain to the U.S.
