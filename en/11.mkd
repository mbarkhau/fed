Types of Warehouse Receipts
===========================

Two kinds of warehouse receipts for deposit banks have developed over
the centuries. One is the regular form of receipt, familiar to anyone
who has ever used any sort of warehouse: a paper ticket in which the
warehouse guarantees to hand over, on demand, the particular product
mentioned on the receipt, e.g., "The Rothbard Bank will pay to the
bearer of this ticket on demand," 10 dollars in gold coin, or Treasury
paper money, or whatever. For deposit banks, this is called a "note" or
"bank note." Historically, the bank note is the overwhelmingly dominant
form of warehouse receipt.

Another form of deposit receipt, however, emerged in the banks of
Renaissance Italy. When a merchant was large-scale and very well-known,
he and the bank found it more convenient for the warehouse receipt to be
invisible, that is, to remain as an "open book account" on the books of
the bank. Then, if he paid large sums to another merchant, he did not
have to bother transferring actual bank notes; he would just write out a
transfer order to his bank to shift some of his open book account to
that of the other merchant. Thus, Signor Medici might write out a
transfer order to the Ricci Bank to transfer 100,000 lira of his open
book account at the Bank to Signor Bardi. This transfer order has come
to be known as a "check," and the open book deposit account at the bank
as a "demand deposit," or "checking account." Note the form of the
contemporary transfer order known as a check: "I, Murray N. Rothbard,
direct the Bank of America to pay to the account of Service Merchandise
100 dollars."

It should be noted that the bank note and the open book demand deposit
are economically and legally equivalent. Each is an alternative form of
warehouse receipt, and each takes its place in the total money supply as
a surrogate, or substitute, for cash. However, the check itself is not
the equivalent of the bank note, even though both are paper tickets. The
bank note itself is the warehouse receipt, and therefore the surrogate,
or substitute for cash and a constituent of the supply of money in the
society. The check is not the warehouse receipt itself, but an order to
transfer the receipt, which is an intangible open book account on the
books of the bank.

I f the receipt-holder chooses to keep his receipts in the form of a
note or a demand deposit, or shifts from one to another, it should make
no difference to the bank or to the total supply of money, whether the
bank is practicing 100-percent or fractional-reserve banking.

But even though the bank note and the demand deposit are economically
equivalent, the two forms will not be equally marketable or acceptable
on the market. The reason is that while a merchant or another bank must
always trust the bank in question in order to accept its note, for a
check to be accepted the receiver must trust not only the bank but also
the person who signs the check. In general, it is far easier for a bank
to develop a reputation and trust in the market economy, than for an
individual depositor to develop an equivalent brand name. Hence,
wherever banking has been free and relatively unregulated by government,
checking accounts have been largely confined to wealthy merchants and
businessmen who have themselves developed a widespread reputation. In
the days of uncontrolled banking, checking deposits were held by the
Medicis or the Rockefellers or their equivalent, not by the average
person in the economy. If banking were to return to relative freedom, it
is doubtful if checking accounts would continue to dominate the
economy.

For wealthy businessmen, however, checking accounts may yield many
advantages. Checks will not have to be accumulated in fixed
denominations, but can be made out for a precise and a large single
amount; and unlike a loss of bank notes in an accident or theft, a loss
of check forms will not entail an actual decline in one's assets.

