The Genesis of Money
====================

It is impossible to understand money and how it functions, and therefore
how the Fed functions, without looking at the logic of how money,
banking, and Central Banking developed. The reason is that money is
unique in possessing a vital historical component. You can explain the
needs and the demand for everything else: for bread, computers,
concerts, airplanes, medical care, etc., solely by how these goods and
services are valued now by consumers. For all of these goods are valued
and purchased for their own sake. But "money", dollars,
francs, lira, etc., is purchased and accepted in exchange not for any
value the paper tickets have *per se* but because everyone
expects that everyone else will accept these tickets in exchange. And
these expectations are pervasive because these tickets have indeed been
accepted in the immediate and more remote past. An analysis of the
history of money, then, is indispensable for insight into how the
monetary system works today.


Money did not and never could begin by some arbitrary social contract,
or by some government agency decreeing that everyone has to accept the
tickets it issues. Even coercion could not force people and institutions
to accept meaningless tickets that they had not heard of or that bore no
relation to any other pre-existing money. Money arises on the free
market, as individuals on the market try to facilitate the vital process
of exchange. The market is a network, a lattice-work of two people or
institutions exchanging two different commodities. Individuals
specialize in producing different goods or services, and then exchanging
these goods on terms they agree upon. Jones produces a barrel of fish
and exchanges it for Smith's bushel of wheat. Both parties make the
exchange because they expect to benefit; and so the free market consists
of a network of exchanges that are mutually beneficial at every step of
the way.

But this system of "direct exchange" of useful goods, or
"barter", has severe limitations which exchangers soon run up
against. Suppose that Smith dislikes fish, but Jones, a fisherman, would
like to buy his wheat. Jones then tries to find a product, say butter,
not for his own use but in order to resell to Smith. Jones is here
engaging in "indirect exchange", where he purchases butter,
not for its own sake, but for use as a "medium", or
middle-term, in the exchange. In other cases, goods are
"indivisible" and cannot be chopped up into small parts to be
used in direct exchange. Suppose, for example, that Robbins would like
to sell a tractor, and then purchase a myriad of different goods:
horses, wheat, rope, barrels, etc. Clearly, he can't chop the tractor
into seven or eight parts, and exchange each part for a good he desires.
What he will have to do is to engage in "indirect exchange",
that is, to sell the tractor for a more divisible commodity, say 100
pounds of butter, and then slice the butter into divisible parts and
exchange each part for the good he desires. Robbins, again, would then
be using butter as a medium of exchange.

Once any particular commodity starts to be used as a medium, this very
process has a spiralling, or snowballing, effect. If, for example,
several people in a society begin to use butter as a medium, people will
realize that in that particular region butter is becoming especially
marketable, or acceptable in exchange, and so they will demand more
butter in exchange for use as a medium. And so, as its use as a medium
becomes more widely known, this use feeds upon itself, until rapidly the
commodity comes into general employment in the society as a medium of
exchange. A commodity that is in general use as a medium is defined as a
*money*.

Once a good comes into use as a money, the market expands rapidly, and
the economy becomes remarkably more productive and prosperous. The
reason is that the price system becomes enormously simplified. A
"price" is simply the terms of exchange, the ratio of the
quantities of the two goods being traded. In every exchange, x amount of
one commodity is exchanged for y amount of another. Take the Smith-Jones
trade noted above. Suppose that Jones exchanges 2 barrels of fish for
Smith's 1 bushel of wheat. In that case, the "price" of wheat
in terms of fish is 2 barrels of fish per bushel. Conversely, the
price of fish in terms of wheat is one-half a bushel per
barrel. In a system of barter, knowing the relative price of anything
would quickly become impossibly complicated: thus, the price of a hat
might be 10 candy bars, or 6 loaves of bread, or 1 /10 of a TV set, and
on and on. But once a money is established on the market, then every
single exchange includes the money-commodity as one of its two
commodities. Jones will sell fish for the money commodity, and will then
"sell" the money in exchange for wheat, shoes, tractors,
entertainment, or whatever. And Smith will sell his wheat in the same
manner. As a result, every price will be reckoned simply in terms of its
"money-price", its price in terms of the common
money-commodity.

Thus, suppose that butter has become the society's money by this market
process. In that case, all prices of goods or services are reckoned in
their respective money-prices; thus, a hat might exchange for 15 ounces
of butter, a candy bar may be priced at 1.5 ounces of butter, a TV set
at 150 ounces of butter, etc. If you want to know how the market price
of a hat compares to other goods, you don't have to figure each relative
price directly; all you have to know is that the money-price of a hat is
15 ounces of butter, or 1 ounce of gold, or whatever the money-commodity
is, and then it will be easy to reckon the various goods in terms of
their respective money-prices.

Another grave problem with a world of barter is that it is impossible
for any business firm to calculate how it's doing, whether it is making
profits or incurring losses, beyond a very primitive estimate. Suppose
that you are a business firm, and you are trying to calculate your
income, and your expenses, for the previous month. And you list your
income: "let's see, last month we took in 20 yards of string, 3
codfish, 4 cords of lumber, 3 bushels of wheat  etc.," and
"we paid out: 5 empty barrels, 8 pounds of cotton, 30 bricks, 5
pounds of beef." How in the world could you figure out how well you
are doing? Once a money is established in an economy, however, business
calculation becomes easy: "Last month, we took in 500 ounces of
gold, and paid out 450 ounces of gold. Net profit, 50 gold ounces."
The development of a general medium of exchange, then, is a crucial
requisite to the development of any sort of flourishing market economy.

In the history of mankind, every society, including primitive tribes,
rapidly developed money in the above manner, on the market. Many
commodities have been used as money: iron hoes in Africa, salt in West
Africa, sugar in the Caribbean, beaver skins in Canada, codfish in
colonial New England, tobacco in colonial Maryland and Virginia. In
German prisoner-of-war camps of British soldiers during World War II,
the continuing trading of CARE packages soon resulted in a
"money" in which all other goods were priced and reckoned.
Cigarettes became the money in these camps, not because of any
imposition by German or British officers or from any sudden agreement:
it emerged "organically" from barter trading in spontaneously
developed markets within the camps.

Throughout all these eras and societies, however, two commodities, if
the society had access to them, were easily able to outcompete the rest,
and to establish themselves on the market as the only moneys. These were
gold and silver.

Why gold and silver? (And to a lesser extent, copper, when the other two
were unavailable.) Because gold and silver are superior in various
"moneyish" qualities–qualities that a good needs to have
to be selected by the market as money. Gold and silver were highly
valuable in themselves, for their beauty; their supply was limited
enough to have a roughly stable value, but not so scarce that they could
not readily be parcelled out for use (platinum would fit into the latter
category); they were in wide demand, and were easily portable; they were
highly divisible, as they could be sliced into small pieces and keep a
*pro rata* share of their value; they could be easily made
homogeneous, so that one ounce would look like another; and they were
highly durable, thus preserving a store of value into the indefinite
future. (Mixed with a small amount of alloy, gold coins have literally
been able to last for thousands of years.) Outside the hermetic
prisoner-of-war camp environment, cigarettes would have done badly as
money because they are too easily manufactured; in the outside world,
the supply of cigarettes would have multiplied rapidly and its value
diminished nearly to zero. (Another problem of cigarettes as money is
their lack of durability.)

Every good on the market exchanges in terms of relevant quantitative
units: we trade in "bushels" of wheat; "packs" of 20
cigarettes; "a pair" of shoelaces; one TV set; etc. These
units boil down to number, weight, or volume. Metals invariably trade
and therefore are priced in terms of *weight*:tons, pounds,
ounces, etc. And so moneys have generally been traded in units of
weight, in whatever language used in that society. Accordingly, every
modern currency unit originated as a *unit of weight of gold or silver*.
Why is the British currency unit called "the pound sterling"?
Because originally, in the Middle Ages, that's precisely what it was: a
pound weight of silver. The "dollar" began in
sixteenth-century Bohemia, as a well-liked and widely circulated
one-ounce silver coin minted by the Count of Schlick, who lived in
Joachimsthal. They became known as Joachimsthalers, or
Schlichtenthalers, and human nature being what it is, they were soon
popularly abbreviated as thalers, later to become
dollars in Spain. When the United States was founded, we
shifted from the British pound currency to the dollar, defining the
dollar as approximately 1 /20 of a gold ounce, or 0.8 silver ounces.
