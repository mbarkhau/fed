Was ist die optimale Geldmenge?
===============================

What Is the Optimum Quantity of Money?
======================================


Die Menge an Geld in einem Gebiet oder einer Gesellschaft, bzw. der
Umfang oder das "Angebot" an Geld ist einfach nur die Summe allen Goldes
oder des Geldes in dieser Gesellschaft oder Region. Ökonomen
beschäftigen sich oft mit den Fragen: Was ist die "optimale" Menge an
Geld, welcher Gesamtbestand an Geld ist zum jetzigen Zeitpunkt
angebracht? Wie schnell sollte dieser Wachsen?.

The total stock, or "supply", or quantity of money in any area
or society at any given time is simply the sum total of all the ounces
of gold, or units of money, in that particular society or region.
Economists have often been concerned with the question: what is the
"optimal" quantity of money, what should the total money stock
be, at the present time? How fast should that total grow"?


Wenn wir diese Fragen genauer betrachten, sollte uns jedoch etwas
merkwürdiges auffallen. Wir stellen uns schließlich nicht die Fragen:
Welche Menge an Pfirsiche ist jetzt oder in der Zukunft das "optimale
Angebot"? Oder Nintendo Spiele? Oder Frauenschuhe? Tatsächlich sind
solche Fragen an und für sich schon Absurd. Eine wesentliche Tatsache in
jeder Wirtschaft ist, dass alle Ressourcen, im Bezug auf menschliche
Bedürfnisse, der Knappheit unterliegen; wenn ein Gut nicht knapp ist, so
ist es im Überfluss vorhanden und es hat einen entsprechenden Marktwert,
wie etwa Luft, nämlich Null. Wir können also *ceteris paribus* sagen,
dass es immer Wünschenswert ist mehr Güter zu haben. Wenn jemand eine
neue Kupfermine findet, oder eine bessere Möglichkeit entdeckt Weizen
oder Stahl zu produzieren, dann stellt die größere Menge der Güter einen
Gewinn für die Gesellschaft dar. Je mehr Güter, desto besser, es sei
denn wir kehren zum Garten Eden zurück; denn dort herrscht keine
natürliche Knappheit und der Lebensstandard ist erhöht. Eben weil diese
Frage derart absurd ist, wird sie praktisch nie gestellt.

If we consider this common question carefully, however, it should strike
us as rather peculiar. How come, after all, that no one addresses the
question: what is the "optimal supply" of canned peaches today
or in the future? Or Nintendo games? Or ladies' shoes? In fact, the very
question is absurd. A crucial fact in any economy is that all resources
are scarce in relation to human wants; if a good were not scarce, it
would be superabundant, and therefore be priced, like air, at zero on
the market. Therefore, other things being equal, the more goods
available to us the better. If someone finds a new copper field, or
discovers a better way of producing wheat or steel, these increases in
supply of goods confer a social benefit. The more goods the better,
unless we returned to the Garden of Eden; for this would mean that more
natural scarcity has been alleviated, and living standards in society
have increased. It is because people sense the absurdity of such a
question that it is virtually never raised.


Nun müssen wir uns aber fragen, warum das Problem einer optimalen
Geldmenge aufkommt? Es hat mit der zuvor betrachteten unerlässlichen
Rolle des Geldes bei jeder Wirtschaft zu tun, wenn sie über einen
primitiven Stand hinauswachsen soll. Eine Besonderheit des Geldes ist,
dass es einer Gesellschaft zwar enorme Vorteile bringt, es jedoch nicht
wie bei allen anderen Gütern, *ceteris paribus*, besser ist mehr davon
zu haben. Denn eine größere Menge anderer Güter sorgt entweder für mehr
Konsumgüter die genutzt werden können oder es sorgt für mehr Ressourcen
und Kapital, welche für die Herstellung einer größeren Menge an
Konsumgüter benutzt werden können. Welchen direkten Vorteil hat jedoch
eine Erhöhung des Angebots an Geld?

But why, then, does an optimal supply of money even arise as a problem?
Because while money, as we have seen, is indispensable to the
functioning of any economy beyond the most primitive level, and while
the existence of money confers enormous social benefits, this by no
means implies, as in the case of all other goods, that, other things
being equal, the more the better. For when the supplies of other goods
increase, we either have more consumer goods that can be used, or more
resources or capital that can be used up in producing a greater supply
of consumer goods. But of what direct benefit is an increase in the
supply of money?


Geld kann man schließlich weder Essen noch für die Produktion
gebrauchen. Die Geldware, mit ihrer Funktion als Geld kann nur benutzt
werden um es gegen Waren und Dienstleistungen einzutauschen, um den
Transfer dieser Güter zu gewährleisten und um wirtschaftliche
Kalkulation zu ermöglichen. Sobald ein Geld sich jedoch auf dem Markt
etabliert hat, muss sein Angebot nicht erhöht werden und solche
Erhöhungen hätten keinen echten gesellschaftlichen Nutzen. Wir wissen
aus der allgemeinen Wirtschaftstheorie, dass die Erhöhung des Angebots
von einem Gut zu einer Verringerung von seinem Preise führt. Solche
Erhöhungen haben bei jedem Produkt außer dem Geld einen
gesellschaftlichen Nutzen, denn sie bedeuten, dass die Produktion und
der Wohlstand aufgrund der Nachfrage von Konsumenten erhöht wurden.
Wenn Stahl, Brot oder Häuser im größeren Umfang und billiger sind als
vorher, ist der Wohlstand aller vergrößert. Das Angebot an Geld zu
erhöhen kann jedoch nichts an der natürlichen Knappheit der Konsum- oder
Kapitalgüter ändern; es sorgt lediglich dafür, dass der Dollar oder Euro
billiger wird, d.h. seine Kaufkraft im Bezug auf alle anderen Waren und
Dienstleistungen wird verringert. Sobald eine Ware auf dem Markt als
Geld etabliert ist, setzten seine Stärken als Tauschmittel und
Instrument zur wirtschaftlichen Kalkulation ein. Die Menge an Dollar zu
erhöhen kann einzig und allein dafür sorgen, dass der Nutzen, also die
Kaufkraft, von jedem einzelnen Dollar beeinträchtigt wird.

Money, after all, can neither be eaten nor used up in production. The
money-commodity, functioning as money, can only be used in exchange, in
facilitating the transfer of goods and services, and in making economic
calculation possible. But once a money has been established in the
market, no increases in its supply are needed, and they perform no
genuine social function. As we know from general economic theory, the
invariable result of an increase in the supply of a good is to lower its
price. For all products except money, such an increase is socially
beneficial, since it means that production and living standards have
increased in response to consumer demand. If steel or bread or houses
are more plentiful and cheaper than before, everyone's standard of
living benefits. But an increase in the supply of money cannot relieve
the natural scarcity of consumer or capital goods; all it does is to
make the dollar or the franc cheaper, that is, lower its purchasing
power in terms of all other goods and services. Once a good has been
established as money on the market, then, it exerts its full power as a
mechanism of exchange or an instrument of calculation. All that an
increase in the quantity of dollars can do is to dilute the
effectiveness, the purchasing-power, of each dollar. Hence, the great
truth of monetary theory emerges: once a commodity is in sufficient
supply to be adopted as a money, no further increase in the supply of
money is needed. *Any* quantity of money in society is
"optimal". Once a money is established, an increase in its
supply confers no social benefit.


Müssen wir daraus schließen, dass bei einer Gold gedeckten Währung,
jeder Abbau und die Produktion von Gold eine Verschwendung ist? Nein,
denn wenn sich die Menge an Gold vergrößert, kann es für seine nicht
geldliche Funktionen eingesetzt werden: mehr Schmuck, Verzierungen,
Zahnfüllungen usw. zu einem günstigeren Preis. Mehr Gold *als Geld*
braucht eine Wirtschaft jedoch nicht. Geld ist demnach einzigartig
unter Waren und Dienstleistungen, denn die Vergrößerungen von seinem
Angebot ist weder von Vorteil noch ist sie nötig; tatsächlich kann solch
eine Vergrößerung lediglich den einzigartigen Wert des Geldes
verwässern: Es Wert zu sein etwas dagegen einzutauschen.

Does that mean that, once gold became money, all mining and production
of gold was a waste? No, because a greater supply of gold allowed an
increase in gold's non-monetary use: more abundant and lower-priced
jewelry, ornaments, fillings for teeth, etc. But more gold *as
money* was not needed in the economy. Money, then, is unique
among goods and services since increases in its supply are neither
beneficial nor needed; indeed, such increases only dilute money's unique
value: to be a worthy object of exchange.

