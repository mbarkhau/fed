Ursprung der Fed: Die Entstehung des Nationalen Bankensysems
============================================================

Origins of the Federal Reserve: The Advent of the National Banking System
=========================================================================


Die praktisch unvermeidliche Einstellung der Banken, gegenüber der
Zentralbank ihres Landes, sollte mittlerweile Glasklar sein. Die
Zentralbank ist ihre Absicherung, ihre Basis und ihr Schutzschirm gegen
den Ansturm der Konkurrenz und der Öffentlichkeit, die versuchen das
Geld zurückzubekommen, welches sie als ihr Eigentum betrachten und von
denen sie meinen es wäre in den Tresoren der Banken. Die Zentralbank
sorgt für das unabdingbare Vertrauen der leichtgläubigen Öffentlichkeit
und hält ihren Ansturm zurück. Die Zentralbank ist für die Banken, der
Kreditgeber letzter Zuflucht und die zentrale Institution zur Bildung
eines Kartells. Mit ihr können alle Banken gemeinsam expandieren und sie
müssen sie sich keine Sorgen machen, dass eine Gruppe von Banken ihre
Reserven an andere verliert, zu einer Kontraktion ihre Kredite gezwungen
wird oder in die Pleite getrieben wird. Die Zentralbank ist fast
unerlässlich für den Wohlstand der Banken; für ihren professionellen
Betrieb als Hersteller neuen Geldes durch die Herausgabe unechter
Obligationen an echtes Bargeld.

It should now be crystal clear what the attitude of commercial banks is
and almost always will be toward the Central Bank in their country. The
Central Bank is their support, their staff and shield against the winds
of competition and of people trying to obtain money which they believe
to be their own property waiting in the banks' vaults. The Central Bank
crucially bolsters the confidence of the gulled public in the banks and
deters runs upon them. The Central Bank is the banks' lender of last
resort, and the cartelizer that enables all the banks to expand together
so that one set of banks doesn't lose reserves to another and is forced
to contract sharply or go under. The Central Bank is almost critically
necessary to the prosperity of the commercial banks, to their
professional career as manufacturers of new money through issuing
illusory warehouse receipts to standard cash.


Ebenso können wir nun ihre Verlogenheit sehen wenn behauptet wird, dass
Geschäftsbanken wie niemand anderes auf Inflation achtet, oder dass
Zentralbanken sich im ewigen Kampf gegen Inflation befinden. Sie sind
vielmehr alle gemeinsamen und als *einzige* für die Inflation in der
Wirtschaft verantwortlich. 

Also we can now see the mendacity of the common claim that private
commercial banks are "inflation hawks" or that Central Banks are
eternally guarding the pass against inflation. Instead, they are all
jointly the inflation-creators, and the *only* inflation-creators, in
the economy.


Freilich heißt das nicht, dass Banken sich niemals über ihre Zentralbank
beschweren. Wie in jeder "Ehe" gibt es ab und zu Spannungen und die
Banken meckern nicht selten über ihren Hirten und Beschützer. Die
Beschwerden sind jedoch fast immer, dass die Zentralbank zu *wenig*
macht: Sie wollen mehr Inflation, oder stärkeren und beständigeren
Schutz. Die Zentralbank führt und berät die Geschäftsbanken zwar, muss
jedoch auch andere, weitestgehend politische Zwänge berücksichtigen.
Selbst wenn ihre Noten wie heute das allgemein gebräuchliche Bargeld
sind, müssen sie sich um die öffentliche Meinung kümmern, sowie um die
üblichen Beschwerden gegen Inflation, oder über die scharfsinnige wenn
auch undifferenzierte Kritik der Öffentlichkeit an der "Macht des
Geldes". Die Einstellung einer Bank gegenüber der Zentralbank und der
Regierung, ähnelt dem Kummer etwa der Klienten des Sozialstaates oder
der Empfänger von Subventionen in der Industrie. Sie beschweren sich
fast überhaupt nicht über die Existenz des Sozialstaates oder der
Subventionen, sondern vor allen Dingen über die angeblichen "Mängel" und
über den Umfang der Ausgaben. Fragt man die Wehklagenden, ob sie denn
die Sozialleistungen oder Subventionen abschaffen möchten, so ist ihnen
der blanke Horror ins Gesicht geschrieben, sofern sie die Frage
überhaupt ernst nehmen. Gleichermaßen, kann man einem Bankier fragen, ob
er für die Abschaffung seiner Zentralbank wäre und eine sehr ähnliches
Schrecken beobachten.[^12]

Now this does not mean, of course, that banks are never griping about
their Central Bank. In every "marriage" there is friction, and there is
often grousing by the banks against their shepherd and protector. But
the complaint, almost always, is that the Central Bank is not inflating,
is not protecting them, intensely or consistently *enough*. The
Central Bank, while the leader and mentor of the commercial banks, must
also consider other pressures, largely political. Even when, as now,
their notes are standard cash, they must worry about public opinion, or
about populist complaints against inflation, or about instinctively
shrewd if often unsophisticated public denunciations of the "money
power." The attitude of a bank toward the Central Bank and government is
akin to general bellyaching by welfare "clients" or by industries
seeking subsidies, against the government. The complaints are almost
always directed, not against the existence of the welfare system or the
subsidy program, but against alleged "deficiencies" in the intensity and
scope of the subventions. Ask the complainers if they wish to abolish
welfare or subsidies and you will see the horror of their response, if
indeed they can be induced to treat the question seriously. In the same
way, ask any bankers if they wish to abolish their Central Bank and the
reaction of horror will be very similar.[^12]


[^12]: Ein weit verbreiteter Aberglaube ist, dass die "Staatsbanken"
    (Banken die von den Bundesstaaten zugelassen wurden) Andrew Jackson
    eine nachhaltige Unterstützung waren, bei der Abschaffung der
    *Second Bank of the United States* – die damalige Zentralbank.
    Wir wollen von der Tatsache absehen, dass es sich hierbei um eine
    vor dem *Peel Act* entstandene Zentralbank handelte, die kein
    Monopol auf die Herausgabe von Banknoten hatte und demnach sowohl
    mit den kommerziellen Banken Konkurrenz war, als auch Reserven für
    ihre Expansion bereitstellte. Hierbei handelt es sich nämlich um
    eine schiere Legende die auf den falschen Annahmen vieler Historiker
    beruht, dass die Banken bei ihrer Expansion durch die *Bank of the
    United States* "eingeschränkt" waren und sie deshalb für ihre
    Abschaffung gewesen sein *müssen*. Im Gegenteil; spätere Historiker
    zeigten, dass die überwältigende Mehrheit der Banken die Zentralbank
    erhalten wollten, wie unsere Analyse es uns erwarten lassen würde.
    Siehe John M. McFaul, *The Politics of Jacksonian Finance* (Ithaca,
    N.Y.: Cornell University Press, 1972), pp. 16–57; und Jean Alexander
    Wilburn, *Biddle's Bank: The Crucial Years* (New York: University
    Press, 1970), pp. 118–19.

[^12]: It is a commonly accepted myth that the "state banks" (the 
    state-chartered private commercial banks) strongly supported Andrew
    Jackson's abolition of the Second Bank of the United States–the
    Central Bank of that time. However (apart from the fact that this
    was a pre-Peel Act Central Bank that did not have a monopoly on bank
    notes and hence competed with commercial banks as well as providing
    reserves for their expansion) this view is sheer legend, based on
    the faulty view of historians that since the state banks were
    supposedly "restrained" in their expansion by the Bank of the United
    States, they *must* have favored its abolition. On the contrary, as
    later historians have shown, the overwhelming majority of the banks
    supported retention of the Bank of the United States, as our analysis
    would lead us to predict. See John M. McFaul, *The Politics of
    Jacksonian Finance* (Ithaca, N.Y.: Cornell University Press, 1972),
    pp. 16–57; and Jean Alexander Wilburn, *Biddle's Bank: The Crucial 
    Years* (New York: University Press, 1970), pp. 118–19.


Im ersten Jahrhundert der Geschichte der amerikanischen Republik, war
Geld und das Bankenwesen grundsätzliche politische Fragen zwischen den
Parteien. Eine Zentralbank war schon zu Beginn der Nation eine heiß
diskutierte Frage der amerikanischen Nation. Immerzu waren die
Verfechter der Zentralbank die unbeirrbaren Nationalisten, die
Befürworter einer ausgedehnten zentralen Regierung. 1781 wurde vom
Kontinentalkongress, noch vor Ende des Unabhängigkeitskrieges mit der
*Bank of North Amerika* (BNA) die erste Zentralbank des Landes
genehmigt. Robert Morris aus Philadelphia, ein Großunternehmer,
führender Nationalist im Kongresses und Finanzminister zur Kriegszeit
brachte den Vorschlag zur Genehmigung ein. Ähnlich wie die *Bank of
England*, gewährte der amerikanische Kongress der BNA ein Monopol auf
die Herausgabe von Papiernoten im gesamten Land. Der größte Teil der
Noten wurde herausgegeben um die neuen Schulden der nationalen Regierung
zu kaufen und die BNA wurde der Verwahrer aller Gelder des Kongresses.
Über die Hälfte des Kapitals der BNA stammte offiziell von dem
Kongress.[^13] Die Noten der BNA konnten angeblich gegen Hartgeld
eingelöst werden, da die BNA jedoch mit dem partiellen Reservensystem
arbeitete, verloren seine Noten mit zunehmender Entfernung des
Hauptstandorts in Philadelphia an Wert. Nach dem Unabhängigkeitskrieg,
verlor Morris seine nationale politische Macht, wurde gezwungen die BNA
schnell in eine reguläre Privatbank umzuwandeln, ohne jegliche
Privilegien der Regierung.

For the first century of the history of the American Republic, money and
banking were crucial policy issues between the political parties. A
Central Bank was a live issue from the beginning of the American nation.
At each step of the way, the champions of the Central Bank were the
enthusiastic Nationalists, the advocates of a Big Central Government. In
fact, in 1781, even before the Revolutionary War was over, the leading
Nationalist, Philadelphia's merchant tycoon Robert Morris, who was
Congress's virtual wartime economic czar, got Congress to charter the
first Central Bank, his own Bank of North America (BNA). Like the Bank
of England, Congress bestowed on Morris's private BNA the monopoly
privilege of issuing paper notes throughout the country. Most of these
notes were issued in the purchase of newly-issued federal debt, and the
BNA was made the depository of all congressional funds. Over half the
capital of the BNA was officially subscribed by Congress.[^13] The BNA
notes were supposedly redeemable in specie, but of course the
fractional-reserve expansion indulged in by the BNA led to the
depreciation of its notes outside its home base in Philadelphia. After
the end of the Revolutionary War, Morris lost his national political
power, and he was forced to privatize the BNA swiftly and to shift it to
the status of a regular private bank shorn of government privilege.


[^13]: Morris konnte zunächst nicht das gesetzlich erforderliche
    Hartgeld als Kapital für die Gründung der BNA aufbringen. Als Ausweg
    "investierte" er im Namen der amerikanischen Regierung einfach 
    von Frankreich an das U.S. Schatzamt geliehenes Hartgeld in seine
    eigene Bank. Über diesen Vorfall und die Geschichte der
    Außeinandersetzung um eine Zentralbank in Amerika, von damals bis
    ins neunzehnte Jahrhundert, siehe "The Minority Report of the U.S.
    Gold Commission of 1981", in der neusten Fassung *The Ron Paul
    Money Book* (Clute, Texas: Plantation Publishing, 1991), pp. 44–136.

[^13]: When Morris failed to raise the specie capital legally required
    to launch the BNA, he simply appropriated specie loaned to the 
    U.S. Treasury by France and invested it for the U.S. government in
    his own bank. On this episode, and on the history of the war over
    a Central Bank in America from then through the nineteenth century,
    see "The Minority Report of the U.S. Gold Commission of 1981,"
    in the latest edition, *The Ron Paul Money Book*(Clute, Texas:
    Plantation Publishing, 1991), pp. 44–136.


Im gesamten ersten Jahrhundert der Republik waren die Befürworter einer
Zentralbank, zunächst die föderalistische Partei in der Tradition von
Hamilton, danach die Whig Partei und schließlich die Republikaner. Immer
waren diese auch die Parteien für eine große zentrale Regierung, hohe
öffentliche Schulden, hohe Schutzzölle, großangelegte öffentliche
Bauvorhaben, und einer frühen Form "öffentlich-privater
Partnerschaften", nämlich Subventionen an Großindustrielle. Schutzzölle
zur Begünstigung inländischer Hersteller, Papiergeld, das partielle
Reservensystem und eine Zentralbank wurden alle von den Nationalisten
als Teil ihrer inflationären und expansiven Politik befürwortet. Sie
begünstigten damit politisch gut verbundene Firmen und Industrien, die
sich als Teil der finanziellen Elite um Philadelphia und New York, nach
dem Sezessionskrieg aber vor allem um New York sammelten.

Throughout the first century of the Republic, the party favoring a
Central Bank, first the Hamiltonian High Federalists, then the Whigs and
then the Republicans, was the party of Big Central Government, of a
large public debt, of high protective tariffs, of large-scale public
works, and of subsidies to large businesses in that early version of
"partnership between government and industry," Protective tariffs were
to subsidize domestic manufactures, while paper money, fractional
reserve banking, and Central Banking were all advocated by the
nationalists as part of a comprehensive policy of inflation and cheap
credit in order to benefit favored businesses. These favorites were
firms and industries that were part of the financial elite, centered
from the beginning through the Civil War in Philadelphia and New York,
with New York assuming first place after the end of that war.


Gegen diese mächtige Gruppe von Nationalisten, stellte sich eine ebenso
mächtige Bewegung, die sich für Laissez-faire, freie Märkte, freien
Handel, sowie minimale und dezentralisierte Regierungen aussprach. Im
Bezug auf das Finanzwesen setzten sie sich für Hartgeld in Form von Gold
und Silber ein und waren gegen jegliche besondere Privilegien für
Banken, die hoffentlich ihre Noten zu 100% mit Reserven decken sollten.
Diese Libertäre waren das Herz und die Seele der Jefferson-Republikaner
und danach der Jacksonian Demokraten. Sie und ihre Wählerschaft waren in
jedem Berufsstand vertreten, außer dem der Bankiers und ihre elitäre
Klienten.

Ranged against this powerful group of nationalists was an equally
powerful movement dedicated to laissez-faire, free markets, free trade,
ultra-minimal and decentralized government, and, in the monetary sphere,
a hard-money system based squarely on gold and silver, with banks shorn
of all special privileges and hopefully confined to 100-percent specie
banking. These hard-money libertarians made up the heart and soul of the
Jeffersonian Democratic-Republican and then the Jacksonian Democratic
party, and their potential constituents permeated every occupation
except those of banker and the banker's favored elite clientele.


Nachdem die *First Bank of the United States* von Hamilton gegründet
wurde, folgte nach dem Krieg in 1812 die *Second Bank of the U.S.*,
welche von Bank-Befürwortern in der Demokraitsch-Republikanischen Partei
gegründet wurde. Nach einem gigantischen Kampf der durch die 1830er
andauerte, konnte Präsident Andrew Jackson schließlich die Zentralbank
abschaffen. Die Jacksonian Demokraten konnten zwar nicht all ihre
Hartgeld Forderungen in den 1840er und 1850er Jahren durchsetzen, vor
allem Aufgrund ihrer zunehmenden Spaltung im Bezug auf die Sklaverei,
sie konnten jedoch zum letzten mal in den Vereinigten Staaten den freien
Handel etablieren, die Zentralbank abschaffen, ebenso wie jede Spur
eines zentralisierten Bankensystems.

After the First Bank of the United States was established by Hamilton,
followed by a Second Bank put in by pro-bank Democrat-Republicans after
the War of 1812, President Andrew Jackson managed to eliminate the
Central Bank after a titanic struggle waged during the 1830s. While the
Jacksonian Democrats were not able to enact their entire hardmoney
program during the 1840s and 1850s because of the growing Democratic
split over slavery, the regime of those decades, in addition to
establishing free trade for the last time in the United States, also
managed to eliminate not only the Central Bank but all traces of
centralized banking.


Die neu in den 1850er gegründete Partei der Republikaner, hatte zwar
viele vorherige Jacksonian Democraten, im Bezug auf wirtschaftliche
Fragen wurde die Politik der Republikander jedoch vor allem durch die
ehemaligen Mitglieder der Whig Partei bestimmt. Nach der Sezession des
Südens nutzten die Republikaner, als praktisch alleinige Partei im
Kongress, ihren Sieg um ihr Programm eines wirtschaftlichen
Nationalismus und Etatismus durchzusetzen. Dieses nationalistische
Programm umfasste: Eine umfangreiche Ausweitung der Macht der zentralen
Regierung, welches sie mit der ersten Einkommenssteuer und hohe Steuern
auf Alkohol und Tabak finanzierten; weiterhin gewährten sie den
transkontinentalen Eisenbahngesellschaften weit reichende
Eigentumsrechte über Land sowie finanzielle Subventionen; darüber hinaus
führten sie wieder die Schutzzölle ein.

While the new Republican Party of the 1850s contained many former
Jacksonian Democrats, the economic agenda of the Republicans was firmly
fixed by the former Whigs in the party. The victorious Republicans took
advantage of the near one-party Congress after the secession of the
South to drive through their cherished agenda of economic nationalism
and statism. This nationalist program included: a huge increase in
central government power fueled by the first income tax and by heavy
taxes on liquor and tobacco, vast land grants as well as monetary
subsidies to new transcontinental railroads; and the reestablishment of
a protective tariff.


In der etatistischen Revolution der Republikaner waren finanzielle
Themen nicht im geringsten betroffen. Um den Krieg gegen den Süden zu
finanzieren, löste die Regierung der Vereinigten Staaten die Golddeckung
auf und druckte unkündbare Obligationen in Form von Papiergeld namens
"Greenback" (technisch gesehen handelte es sich um U.S. Noten). Das
unkündbare Papiergeld sank nach zwei Jahren auf einen Marktwert von 50
Cent im Vergleich zu einem Dollar aus Gold, sodass sich die zentrale
Regierung zur Finanzierung des Krieges, zunehmend auf die Herausgabe
öffentlicher Schulden verließ.

Not the least of the Republican statist revolution was effected on the
monetary and financial front. To finance the war effort against the
South, the federal government went off the gold standard and issued
irredeemable fiat paper money, called "Greenbacks" (technically, U.S.
Notes). When the irredeemable paper, after two years, sank to 50 cents
on the dollar on the market in terms of gold, the federal government
turned increasingly to issuing public debt to finance the war.


Das Programm der Whig-Republikander konnte demnach die Hartgeld
Tradition der Jefferson und Jackson Demokraten ebenso abstoßen wie ihre
Feindseligkeit gegenüber öffentliche Schulden. (Sowohl Jefferson als
auch später Jackson schafften es zu ihrer Regierungszeit, zum letzten
Mal in der Geschichte von Amerika, die Schulden der U.S. Regierung *ab
zu bezahlen!*). Darüber hinaus fühlten sich die Republikaner zwar nicht
in der Lage eine Zentralbank zurückzubringen, sie schafften es jedoch
das relativ freie und nicht-inflationäre Bankenwesen abzuschaffen, dass
sich in der Zeit nach Jackson etabliert hatte. An ihrer Stelle setzten
sie das neue *National Banking System*, welches die Banken des Landes
zentralisierte und im Wesentlichen den Übergang zu einer Zentralbank
schuf.

Thus, the Republican-Whig program managed to dump the traditional
Jefferson-Jackson Democratic devotion to hard money and gold, as well as
their hatred of the public debt. (Both Jefferson, and later Jackson, in
their administrations, managed, for the last time in American history,
*to pay off* the federal public debt!) In addition, while the
Republicans did not yet feel strong enough to bring back a Central Bank,
they effectively put an end to the relatively free and non-inflationary
banking system of the post-Jacksonian era, and created a new National
Banking System that centralized the nation's banks, and established what
amounted to a halfway house toward Central Banking.


Die Banken der Bundesstaaten waren seit Anfang des Krieges zufrieden:
Mit Greenbacks als Reserven sie konnten aus Noten und
Einlagen-/Guthabenkonten eine Pyramide errichten. Auf diese Art konnten
sie mit dem partiellen Reservensystem Expansion betreiben und für eine
Ausweitung der Geldmenge sorgen. Später, im Sezessionskrieg, wurde das
*National Banking System* durch Bankengesetze aus den Jahren 1863, 1864
und 1865, von der U.S. Regierung eingeführt. Anders als der *Peel Act*,
indem die *Bank of England* als einzige das Monopol auf Banknoten hatte,
gewährte die *National Banking* Gesetzgebung dieses Monopol einer neuen
Kategorie von "Nationalbanken", die von der U.S. Regierung eingeführt
wurden; die bestehenden Staatsbanken der Bundesstaaten durften nicht
länger Noten herausgeben und waren auf die Herausgabe von
Einlagen/Bankguthaben beschränkt.[^14] Mit anderen Worten, um Bargeld
bzw. Papiernoten zu bekommen, mussten die Staatsbanken ihre Reserven als
Einlagen-/Guthabenkonten bei den Nationalbanken halten. Solches Bargeld
brauchten sie vor allem um die Einlagen/Guthaben ihrer Kunden
ausbezahlen zu können, wozu sie es zur gegebenen Zeit von ihren Konten
bei den Nationalbanken abheben mussten. Die Nationalbanken wurden
ihrerseits in einer dreiteiligen Hierarchie aufgestellt. Auf der
obersten Ebene waren die führenden "central reserve city" Banken die
ursprüngliche auf große Banken in New York beschränkt waren; unter ihnen
waren die "reserve city" Banken, die in Städte mit einer Bevölkerung von
über 500.000 Menschen stationiert waren; auf der untersten Ebene waren
schließlich die übrigen "country" Banken. Die neue Gesetzgebung hatte
eine Funktion, die vor allem von den Whig Regierungen der Bundesstaaten
vorangetrieben wurde, insbesondere in New York und Michigan in den
1940er Jahren: Gesetzlich erforderliche Mindestreserven für Banken, zur
Deckung ihrer Noten und der Einlagen/Guthaben ihrer Kunden. Die
Mindestreserven schufen ein Instrument um vom oberen Teil der
Bankenhierarchie aus, die unteren Teile zu kontrollieren. Es war dabei
entscheidend, die Banken der unteren Ebenen dazu zu bringen, ihre
Reserven möglichst nicht in das gesetzliche Bargeld (Gold, Silber, oder
Greenbacks) zu halten, sondern in Form von Einlagen-/Guthaben in Konten
bei den Banken der höheren Ebenen.

The state banks had been happy, from the beginning of the war, to
pyramid an expansion of fractional-reserve notes and demand deposits on
top of the increase of federal green-backs, thus expanding the total
supply of money. Later in the Civil War, the federal government created
the National Banking System, in the Bank Acts of 1863, 1864, and 1865.
Whereas the Peel Act had granted to one Bank of England the monopoly of
all bank notes, the National Banking Acts granted such a monopoly to a
new category of federally chartered "national banks"; the existing state
banks were prohibited from any issue of notes, and had to be confined to
issuing bank deposits.[^14] In other words, to obtain cash, or paper
notes, the state banks had to keep their own deposit accounts at the
national banks, so as to draw down their accounts and obtain cash to
redeem their customers' deposits if necessary. For their part, the
national banks were established in a centralized tripartite hierarchy.
At the top of the hierarchy were leading "central reserve city" banks, a
category originally confined to large banks in New York City; beneath
that were "reserve city" banks, in cities of over 500,000 population;
and beneath those were the rest, or "country banks." The new legislation
featured a device pioneered by Whig state governments, especially New
York and Michigan, in the 1840s: legal minimum fractional-reserve
requirements of bank reserves to their notes and deposits. The reserve
requirements fashioned an instrument of control by the upper strata of
the banking hierarchy over the lower. The crucial point was to induce
the lower tiers of banks to keep much of their reserves, not in legal
cash (gold, silver, or greenbacks) but in demand deposit accounts in the
higher tier banks.


[^14]: Der Verbot wurde, streng genommen, durch ein unerträgliche Steuer
    in Höhe von 10% erreicht. Diese wurde auf Noten der Staatsbanken
    von dem Kongress im Frühling von 1865 erhoben, als die Banken der
    Bundesstaaten die Hoffnungen der Republikaner zerstörten. Sie hatten
    sich gesträubt an dem *National Banking System* teilzunehmen, dass
    in den beiden vorhergehenden Jahren aufgestellt worden war.

[^14]: Strictly, this prohibition was accomplished by a prohibitive
    10-percent tax on state bank notes, levied by Congress in the spring
    of 1865 when the state banks disappointed Republican hopes by
    failing to rush to join the National Banking structure as set up in
    the two previous years.


Die "country" Banken mussten eine Mindestreserve von 15% im Verhältnis
zu ihren Noten und ausstehenden Einlagen/Guthaben halten. Von diesen 15%
mussten lediglich 40% in Form von Bargeld sein; das Übrige konnte
entweder als Einlagen/Guthaben bei den "reserve city" oder "central
reserve city" Banken gehalten werden. Die "reserve city" Banken mussten
wiederum eine Mindestreserve von 25% halten und konnten nicht mehr als
die Hälfte dieser Reserven in Form von Bargeld halten; die andere Hälfte
musste als Einlagen/Guthaben bei den "central reserve city" Banken
gehalten werden, die ebenfalls eine Mindestreserve von 25% hatten. Diese
verschiedenen Nationalbanken sollten durch ein Kontrolleur, dem
"comptroller of the currency" genehmigt werden, der von dem Schatzamt
der U.S. Regierung eingesetzt wurde. Um eine Genehmigung zu erhalten,
musste eine Bank viel Kapital aufbringen, je höher in der Hierarchie,
desto mehr. So mussten "country" Banken mindestens $50.000 an Kapital
aufbringen und die "reserve city" Banken waren mit der Maßgabe
konfrontiert, mindestens $200.000 an Kapital aufzubringen.

Thus, the country banks had to keep a minimum ratio of 15 percent of
reserves to their notes and demand deposits outstanding. Of that 15
percent, only 40 percent had to be in cash; the rest could be in the
form of demand deposits at either reserve city or central reserve city
banks. For their part, the reserve city banks, with a minimum reserve
ratio of 25 percent, could keep no more than half of these reserves in
cash; the other half could be kept as demand deposits in central reserve
city banks, which also had to keep a minimum reserve ratio of 25
percent. These various national banks were to be chartered by a federal
comptroller of the currency, an official of the Treasury Department. To
obtain a charter, a bank had to obey high minimum capital requirements,
requirements rising through the hierarchical categories of banks. Thus,
the country banks needed to put up at least $50,000 in capital, and the
reserve city banks faced a capital requirement of $200,000.


Vor dem Sezessionskrieg, konnte jede Staatsbank lediglich auf die
eigenen Gold- und Silberreserven mit Noten und Einlagen/Guthaben
expandieren. Jede übermäßige Expansion würde die Bank in Gefahr bringen
pleite zu gehen, sobald andere Banken oder die Öffentlichkeit anfingen
sie zur Zahlung aufzufordern. Jede Bank musste auf ihrer eigenen
Grundlage bestehen. Desweiteren, konnte jede Bank die verdächtigt wurde
ihre Obligationen nicht erfüllen zu können, beobachten wie ihre Noten
verglichen mit Gold oder den Noten anderer sicherer Banken, auf dem
Markt an Wert verloren.

Before the Civil War, each state bank could only pyramid notes and
deposits upon its own stock of gold and silver, and any undue expansion
could easily bring it down from the redemption demands of other banks or
the public. Each bank had to subsist on its own bottom. Moreover, any
bank suspected of not being able to redeem its warehouse receipts, found
its notes depreciating on the market compared either to gold or to the
notes of other, sounder banks.


Nachdem das *National Banking System* aufgestellt wurde, mussten
einzelne Banken auffälligerweise *nicht* alleine bestehen und als
einzige für ihre Schulden geradestehen. Die U.S. Regierung hatte eine
hierarchische Struktur geschaffen, bei dem vier umgedrehte Pyramiden
aufeinander gestapelt wurden, jede ruhend auf eine kleinere, engere
Pyramide. Die unterste Ebene bildeten eine Handvoll äußerst große,
"central reserve city" Banken, hauptsächlich von Wall Street. Auf ihre
Reserven aus Gold, Silber und Greenbacks, expandierten die Wall Street
Banken ihre Noten und Einlagen-/Guthabenkonten im Verhältnis von 4:1.
Auf ihre Reserven in Form von Einlagen/Guthaben bei den "central reserve
city" Banken, konnten die "reserve city" Banken um 4:1 expandieren, und
die "country" Banken konnten wiederum ihre Obligationen im Verhältnis
von 6,67:1 auf ihre Reserven bei den "reserve city" Banken expandieren.
Schließlich kamen die Banken der Bundesstaaten, die nach dem Zwang
Reserven bei den Nationalbanken zu halten, *ihre* Kredite und Noten auf
den Einlagen/Guthaben bei den "country" oder "reserve city" Banken
expandieren konnten.

After the installation of the National Banking System, however, each
bank was conspicuously *not* forced to stand on its own and be
responsible for its own debts. The U. S. Government had now fashioned a
hierarchical structure of four inverse pyramids, each inverse pyramid
resting on top of a smaller, narrower one. At the bottom of this
multi-tiered inverse pyramid were a handful of very large, central
reserve city, Wall Street banks. On top of their reserves of gold,
silver, and greenbacks, the Wall Street Banks could pyramid an expansion
of notes and deposits by 4:1. On their deposits at the Central Reserve
City banks, the reserve city banks could pyramid by 4:1, and then the
country banks could pyramid their warehouse receipts by 6.67:1 on top of
their deposits at the reserve banks. Finally, the state banks, forced to
keep deposits at national banks, could pyramid *their* expansion of
money and credit on top of their deposit accounts at the country or
reserve city banks.


Um zu verhindern, dass die Noten verschiedener Banken wegen
Kreditexpansion auf dem Markt an Wert verloren, zwang der Kongress alle
Nationalbanken dazu ihre jeweiligen Noten im Nennwert zu akzeptieren.
Die U.S. Regierung machte die Noten von Nationalbanken praktisch zum
gesetzlichen Zahlungsmittel, indem sie solche Noten für Steuern im
Nennwert akzeptierte. Zweigstellen waren für Banken wie auch vor dem
Sezessionskrieg, weiterhin verboten, sodass eine Bank lediglich an ihrer
Hauptgeschäftsstelle ihre Noten ausbezahlen musste. Darüber hinaus,
machte die U.S. Regierung es schwierig Hartgeld zu bekommen, indem sie
die Kontraktion (d.h. netto Auszahlungen) der Noten von Nationalbanken,
auf $3 Millionen beschränkte.

To eliminate the restriction on bank credit expansion of the
depreciation of notes on the market, the Congress required all the
national banks to accept each other's notes at par. The federal
government conferred quasi-legal tender status on each national bank
note by agreeing to accept them at par in taxes, and branch banking
continued to be illegal as before the Civil War, so that a bank was only
required to redeem notes in specie at the counter at its home office. In
addition, the federal government made redemption in specie difficult by
imposing a $3 million per month maximum limit on the contraction (i.e.,
net redemption) of national bank notes.


So wurde dem Land ein neues, zentralisiertes, weit instabileres und
inflationäres Bankensystem aufgebürdet. Auf den Reserven der Wall Street
Banken konnte Inflation betrieben werden und das wurde es auch. Indem
sie am Boden der Pyramide waren, konnten die Wall Street Banken das Geld
und die Kredite der Nation kontrollieren und um ein vielfaches
expandieren. Unter der Deckung von einem "Kriegsnotstand", hatten die
Republikaner das Bankensystem der Nation auf Dauer verändert. Es wurde
von ein relativ stabiles und dezentralisiertes System in ein
inflationäres und zentral auf Wall Street kontrolliertes System
umgewandelt.

Thus, the country was saddled with a new, centralized, and far more
unsound and inflationary banking system that could and did inflate on
top of expansion by Wall Street banks. By being at the bottom of that
pyramid, Wall Street banks could control as well as generate a multiple
expansion of the nation's money and credit. Under cover of a "wartime
emergency," the Republican Party had permanently transformed the
nation's banking system from a fairly sound and decentralized one into
an inflationary system under central Wall Street control.


Die Demokraten im Kongress, mit ihrer Hingabe für Hartgeld, hatte sich
fast bis zum letzten Man gegen das *National Banking System* gestellt.
Die Demokraten brauchten fast ein Jahrzehnt um sich politisch von dem
Sezessionskrieg zu erholen und sie setzten in ihrer Geldpolitik zu
dieser Zeit, all ihre Kraft dazu ein um die Greenback Inflation zu
beenden und zu der Golddeckung zurückzukehren. Diesen Sieg konnten sie
schließlich, trotz der starken Gegenwehr der Republikaner, im Jahre 1879
feiern. Besonders aktive Befürworter der Greenback Inflation war die
industrielle und finanzielle Elite unter den Radikalen der Republikaner:
Die Gesellschafter der Eisen und Stahlindustrie sowie die Betreiber der
großen Eisenbahnlinien. Tatsächlich war es die Panik von 1873, die bei
den nationalistischen Bankiers, den Großindustriellen, sowie den
subventionierten und überdehnte Eisenbahnlinien für Demut und den
Verlust ihrer politischen Macht sorgte, sodass der Sieg von Gold möglich
wurde. Die Panik war die unausweichliche Folge von einem Boom, der von
dem neuen Bankensystem, durch die in ihm erzeugte Inflation,
herbeigeführt wurde. Finanziell bestimmende und mächtige Personen wie
etwa Jay Cooke, der alleinige Bürge für Staatsanleihen aus dem
Sezessionskrieg, der Architekt vom *National Banking Sytem*, wurde in
einem Fall poetischer Gerechtigkeit, durch die Panik in den Ruin
getrieben. Selbst nach 1879 wurde Gold jedoch immer wieder durch
Versuche bedroht, Silber als Hartgeld zurückzubringen oder zusätzlich zu
Gold zu verwenden. Es dauerte bis 1900, bevor die Bedrohung durch Silber
zurückgedrängt wurde und Gold sich als alleinige Deckung und Standard
etabliert hatte. Bis es soweit war, waren die Demokraten jedoch nicht
länger die Partei für Hartgeld und sie waren sogar eher als die
Republikaner, Befürworter von Inflation. Inmitten all dieser Kämpfe um
die grundlegende Währungseinheit, hatten die immer weniger werdenden
Hartgeld Demokraten weder Fähigkeit noch das Bedürfnis, sich darum zu
bemühen, ein freies und dezentralisiertes Bankensystem, wie es vor dem
Sezessionskrieg bestanden hatte, wiederherzustellen.

The Democrats in Congress, devoted to hard money, had opposed the
National Banking System almost to a man. It took the Democrats about a
decade to recover politically from the Civil War, and their monetary
energies were devoted during this period to end greenback inflationism
and return to the gold standard, a victory which came in 1879 and which
the Republicans resisted strongly. Particularly active in pushing for
continued greenback inflation were the industrial and financial power
elite among the Radical Republicans: the iron and steel industrialists
and the big railroads. It was really the collapse of nationalist
bankers, tycoons, and subsidized and over-expanded railroads in the
mighty Panic of 1873 that humbled their political and economic power and
permitted the victory of gold. The Panic was the consequence of the
wartime and post-war inflationary boom generated by the new Banking
System. And such dominant financial powers as Jay Cooke, the monopoly
underwriter of government bonds from the Civil War on, and the main
architect of the National Banking System, was, in a fit of poetic
justice, driven into bankruptcy by the Panic. But even after 1879, gold
was still challenged by inflationist attempts to bring back or add
silver to the monetary standard, and it took until 1900 before the
silver threat was finally beaten back and gold established as the single
monetary standard. Unfortunately, by that time, the Democrats had lost
their status as a hard-money party, and were becoming more inflationist
than the Republicans. In the midst of these struggles over the basic
monetary standard, the dwindling stock of hard-money Democrats had
neither the ability nor the inclination to try to restore the free and
decentralized banking structure as it had existed before the Civil War.

