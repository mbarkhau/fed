Die Zentralbank
===============

Enter the Central Bank
======================


Die erste Zentralbank entstand in England mit der Gründung der "Bank of
England" in 1694. Im Laufe der nächsten beiden Jahrhunderte kopierten
andere große Nationen diese Institution und sie erreichte ihre heute
übliche Form im Jahre 1844 mit dem "Peel Act" aus England. Die
Vereinigten Staaten erhielt mit dem "Federal Reserve System" im Jahr
1913 als letzte große Nation den fragwürdigen Segen einer Zentralbank.

Central Banking began in England, when the Bank of England was chartered
in 1694. Other large nations copied this institution over the next two
centuries, the role of the Central Bank reaching its now familiar form
with the English Peel Act of 1844. The United States was the last major
nation to enjoy the dubious blessings of Central Banking, adopting the'
Federal Reserve System in 1913.


Die Zentralbank befand sich in Privatbesitz, zumindest bis sie Mitte des
zwanzigsten Jahrhunderts praktisch nationalisiert wurde. Sie war jedoch
schon immer eng an die zentrale Regierung gebunden. Zentralbanken hatten
schon immer zwei bedeutende Rollen: (1) die Haushaltsdefizite der
Regierung zu finanzieren; und (2) ein Kartell zwischen den privaten
Geschäftsbanken des Landes zu etablieren, damit diese bei ihrer
Kreditexpansion bzw. ihrem Drang zu fälschen, nicht länger durch diese
zwei Schranken begrenzt sind: Ein möglicher Vertrauensverlust der zum
*Bank Run* führt; und der Verlust von Reserven sollte eine Bank ihre
Kredite zu sehr expandieren. Auf dem Markt sind Kartelle, selbst wenn
sie zum Vorteil jeder Firma sind, sehr schwer aufrechtzuerhalten, es sei
denn sie werden von der Regierung durchgesetzt. Teilreserve Banken auf
einem freien Markt, werden in ihrer inflationären Kreditexpansion durch
die genannte grundsätzlichen Schranken begrenzt, und eine Zentralbank
kann helfen diese aufzulockern oder zu beseitigt.

The Central Bank was privately owned, at least until it was generally
nationalized after the mid-twentieth century. But it has always been in
close cahoots with the central government. The Central Bank has always
had two major roles: (1) to help finance the government's deficit; and
(2) to cartelize the private commercial banks in the country, so as to
help remove the two great market limits on their expansion of credit, on
their propensity to counterfeit: a possible loss of confidence leading
to bank runs; and the loss of reserves should any one bank expand its
own credit. For cartels on the market, even if they are to each firm's
advantage, are very difficult to sustain unless government enforces the
cartel. In the area of fractional-reserve banking, the Central Bank can
assist cartelization by removing or alleviating these two basic
free-market limits on banks' inflationary expansion credit.


Wesentlich ist auch zu beachten, dass die Bank of England eingerichtet
wurde um das riesige Haushaltsdefizit von England zu finanzieren.
Regierungen leiden seit jeher und überall an einer chronischen Knappheit
von Geld, die viel schwerwiegender ist als bei Privatpersonen oder
Firmen. Der Grund dafür ist einfach: Anders als bei Privatpersonen oder
Firmen, produzieren Regierungen nichts von Wert und sie haben demnach
nichts was sie verkaufen könnten.[^10] Regierungen können nur auf eine
einzige Art Geld bekommen, nämlich indem sie es einem anderen wegnehmen,
weshalb sie immer nach neuen und ausgeklügelten Methoden suchen um das
Geld anderer in ihre Griffel zu bekommen. Die übliche Methode ist durch
Steuern; und zumindest bis zum zwanzigsten Jahrhundert waren Steuern
äußerst unbeliebt. So unbeliebt, dass eine Steuererhöhung oder die
Einführung einer neuen Steuer, die Regierung wahrscheinlich zum Einsturz
durch eine Revolution bringen würde.

It is significant that the Bank of England was launched to help the
English government finance a large deficit. Governments everywhere and
at all times are short of money, and much more desperately so than
individuals or business firms. The reason is simple: unlike private
persons or firms, who obtain money by selling needed goods and services
to others, governments produce nothing of value and therefore have
nothing to sell.[^10] Governments can only obtain money by grabbing it
from others, and therefore they are always on the lookout to find new
and ingenious ways of doing the grabbing. Taxation is the standard
method; but, at least until the twentieth century, the people were very
edgy about taxes, and any increase in a tax or imposition of a new tax
was likely to land the government in revolutionary hot water.


[^10]: Es gibt unwesentliche Ausnahmen: Bewundernswert kleine
    Regierungen, wie Monaco oder Liechtenstein, stellen wunderschöne
    Briefmarken aus, die für Sammler sehr begehrenswert sind. Manchmal
    greifen sich Regierungen bestimmte Ressourcen (z.B. ein Wald) oder
    ein Monopol auf eine Dienstleistung um etwas zu verkaufen (z.B. die
    Post) oder sie versteigern ein Monopol auf bestimmte Güter. Diese
    sind jedoch schwerlich Ausnahmen zu der ewigen und gewalttätigen
    Suche von Regierungen nach Einnahmen.

[^10]: A minor exception: when admirably small governments such as
    Monaco or Liechtenstein issue beautiful stamps to be purchased
    by collectors. Sometimes, of course, governments will seize and
    monopolize a service or resource and sell their products (e.g.,
    a forest) or sell the monopoly rights to its production, but
    these are scarcely exceptions to the eternal coercive search
    for revenue by government.


Nachdem die Druckerpresse erfunden wurde, war es nur eine Frage der Zeit
bis Regierungen damit anfingen Geld zu "fälschen" bzw. Papiergeld als
Ersatz für Gold und Silber zu drucken. Ursprünglich konnte man das
Papier gegen diese Metalle einlösen, bzw. Regierungen versprachen ihre
jeweilige Geldeinheit entspräche eine bestimmte Menge an Gold.
Letztendlich wurden diese Versprechen jedoch gebrochen und der Dollar,
der Pfund, die Mark etc. wurden nichts weiter als von Regierungen
herausgegebene Scheine. In der westlichen Welt war die britische Kolonie
namens Massachusetts im Jahr 1690, die erste Regierung die Papiergeld
herausgab.[^11]

After the discovery of printing, it was only a matter of time until
governments began to "counterfeit" or to issue paper money as a
substitute for gold or silver. Originally the paper was redeemable or
supposedly redeemable in those metals, but eventually it was cut off
from gold so that the currency unit, the dollar, pound, mark, etc.
became names for independent tickets or notes issued by government
rather than units of weight of gold or silver. In the Western world, the
first government paper money was issued by the British colony of
Massachusetts in 1690.[^11]


[^11]: Die Druckerei wurde zuerst im alten China entwickelt und es ist
    deshalb nicht weiter verwunderlich, dass die ersten Regierungen die
    Papiergeld druckten aus China, Mitte des achten Jahrhunderts
    stammten. Siehe Gordon Tullock: "Paper Money–A Cycle in Cathay," 
    *Economic History Review* 9, no. 3 (1957): 396.

[^11]: Printing was first developed in ancient China, and so it should
    come as no surprise that the first government paper money arrived
    in mid-eighth century China. See Gordon Tullock, "Paper Money–A
    Cycle in Cathay," *Economic History Review* 9, no. 3 (1957): 396.


Die 90er Jahre des siebzehnten Jahrhunderts waren für die englische
Regierung eine besonders schwierige Zeit. Vier Jahrzehnte Revolution und
Bürgerkrieg, die im Wesentlichen wegen und gegen hohe Steuern geführt
wurden waren gerade zu Ende gegangen, und die neue Regierung sah sich
kaum in der Lage, der Bevölkerung eine neue Runde hoher Steuern
aufzubürden. Dennoch hatte die Regierung Ambitionen viele Länder zu
erobern, insbesondere das mächtige französische Imperium; eine Leistung
die eine ungeheure Erhöhung von Ausgaben bedingte. Den Weg fiskalischer
Defizite war den Engländern jedoch versperrt, da die Regierung erst vor
kurzem ihre Kreditwürdigkeit zerstört hatte, indem es sich weigerte über
die Hälfte seiner Schulden zurückzuzahlen. Diese Erklärung des
Staatsbankrotts brachte auch viele Kapitalisten im Königreich in die
Insolvenz, die ihre Ersparnisse der Regierung anvertraut hatten. Wer
wäre da noch bereit dem englischen Staat Geld zu leihen?

The 1690s were a particularly difficult time for the English government.
The country had just gone through four decades of revolution and civil
war, in large part in opposition to high taxes, and the new government
scarcely felt secure enough to impose a further bout of higher taxation.
And yet, the government had many lands it wished to conquer, especially
the mighty French Empire, a feat that would entail a vast increase in
expenditures. The path of deficit spending seemed blocked for the
English since the government had only recently destroyed its own credit
by defaulting on over half of its debt, thereby bankrupting a large
number of capitalists in the realm, who had entrusted their savings to
the government. Who then would lend anymore money to the English State?


Bei dieser schwierigen Gabelung, wurde das Parlament von einem
Konsortium angesprochen, allen voran von dem schottischen Botaniker
William Patterson. Das Konsortium würde eine "*Bank of England*"
gründen, die genügend (angeblich gegen Gold oder Silber zahlbare)
Banknoten drucken würde, um das Haushaltsdefizit der Regierung zu
finanzieren. Wer braucht sich schon auf freiwillige Ersparnisse zu
verlassen, wenn er einfach den Geldhahn aufdrehen kann? Als
Gegenleistung würde die Regierung alle ihre Einlagen bei der neuen Bank
aufbewahren. Mit ihrer Eröffnung im Juli 1694, begann die *Bank of
England* eilig die enorme Summe von £760.000 herauszugeben, wovon das
Meiste benutzt wurde um Schuldscheine von der Regierung zu kaufen. Es
dauerte weniger als zwei Jahre bis die zu begleichenden Noten in Höhe
von £765.000 nur noch durch £36.000 an Hartgeld gedeckt waren. Bei einem
*Bank Run* wurde Hartgeld gefordert, die Bank in die Knie gezwungen und
in den Ruin getrieben. Die englische Regierung eilte jedoch zur Hilfe
und erlaubte der *Bank of England*, in dem ersten *Bail-Out* von vielen,
ihre Zahlungen einzustellen, d.h. ihre Obligation Gold gegen Banknoten
zu tauschen nicht zu wahren, während sie *ihre* Schuldner dazu zwingen
konnte in voller Höhe zu zahlen. Die Zahlung von Hartgeld wurde zwei
Jahre später wieder aufgenommen, ab diesem Zeitpunkt erlaubte die
Regierung der *Bank of England* jedoch immer wieder bei
Zahlungsschwierigkeiten die Zahlungen einzustellen und dennoch weiterhin
in Betrieb zu bleiben.

At this difficult juncture, Parliament was approached by a syndicate
headed by William Paterson, a Scottish promoter. The syndicate would
establish a Bank of England, which would print enough bank notes,
supposedly payable in gold or silver, to finance the government deficit.
No need to rely on voluntary savings when the money tap could be turned
on! In return, the government would keep all of its deposits at the new
bank. Opening in July 1694, the Bank of England quickly issued the
enormous sum of £760,000, most of which was used to purchase government
debt. In less than two years time, the bank's outstanding notes of
£765,000 were only backed by £36,000 in cash. A run demanding specie
smashed the bank, which was now out of business. But the English
government, in the first of many such bailouts, rushed in to allow the
Bank of England to "suspend specie payments," that is, to cease its
obligations to pay in specie, while yet being able to force *its*
debtors to pay the bank in full. Specie payments resumed two years
later, but from then on, the government allowed the Bank of England to
suspend specie payment, while continuing in operation, every time it got
into financial difficulties.


Nach dem ersten *Bail-Out*, brachte die *Bank of England* das Parlament
im Jahr 1697 dazu, die Gründung neuer kommerzieller Banken in England zu
verbieten. Mit anderen Worten, es konnte keine andere Geschäftsbank mit
der *Bank of England* in Konkurrenz treten. Darüber hinaus wurde die
Todesstrafe auf die Fälschung ihrer Banknoten verhängt. Ein Jahrzehnt
später wurde die Regierung dazu bewegt der *Bank of England*, praktisch
gesehen ein Monopol auf die Herausgabe von Banknoten zu geben.
Insbesondere nach 1708 war es für jede Firma, außer der *Bank of
England*, gesetzlich verboten Papiergeld herauszugeben und die
Herausgabe jeglicher Noten, durch Bank-Partnerschaften mit mehr als
sechs beteiligten Personen, wurde ebenfalls verboten.

The year following the first suspension, in 1697, the Bank of England
induced Parliament to prohibit any new corporate bank from being
established in England. In other words, no other incorporated bank could
enter into competition with the Bank. In addition, counterfeiting Bank
of England notes was now made punishable by death. A decade later, the
government moved to grant the Bank of England a virtual monopoly on the
issue of bank notes. In particular, after 1708, it was unlawful for any
corporation other than the Bank of England to issue paper money, and any
note issue by bank partnerships of more than six persons was also
prohibited.


Zentralbanken, in ihrer heutigen Form, entstanden mit dem "Peel Act" im
Jahre 1844. Der *Bank of England* wurde das absolute Monopol auf die
Herausgabe aller Banknoten in England gewährt. Diese Noten konnten
wiederum gegen Gold eingelöst werden. Private Geschäftsbanken durften
nur noch Einlagekonten betreiben. Dies hatte zur Folge, dass die Banken
bei der *Bank of England* ihrerseits Konten haben mussten, damit sie das
von der Öffentlichkeit geforderte Bargeld ausbezahlen konnten. Im
wesentlichen konnten Bankeinlagen gegen Noten der *Bank of England*
ausgezahlt werden und diese wiederum gegen Gold. Das Bankensystem nahm
die Form einer doppelt invertierten Pyramide an. Bei der unteren
Pyramide, benutzte die *Bank of England* das Teilreservensystem um
gefälschte Obligationen für Gold–d.h. Noten und Einlagekonten der
Geschäftsbanken–herauszugeben, die weit über ihre Goldreserven
hinausgingen. Die Geschäftsbanken benutzten ihrerseits das
Teilreservensystem, um in der zweiten Pyramide über der *Bank of
England* auf *ihre* Reserven, bzw. auf ihre Einlagen bei der *Bank of
England*, Kreditexpansion zu betreiben. Es ist klar, sobald Groß
Brittanien sich von der Golddeckung löste, zum ersten Mal im Ersten
Weltkrieg und schlussendlich im Jahr 1931, konnten die Noten der *Bank
of England* als Fiatgeld dienen und die privaten Banken weiterhin ihre
Pyramide auf ihre Reserven bei der *Bank of England* aufbauen. Der große
Unterschied ist nun, dass die Golddeckung die Kreditexpansion der
Zentralbank nicht länger Einhalt gebieten konnte, d.h. die unbegrenzte
Fälschung von Noten und Einlangen wurde möglich.

The modern form of Central Banking was established by the Peel Act of
1844. The Bank of England was granted an absolute monopoly on the issue
of all bank notes in England. These notes, in turn, were redeemable in
gold. Private commercial banks were only allowed to issue demand
deposits. This meant that, in order to acquire cash demanded by the
public, the banks had to keep checking accounts at the Bank of England.
In effect, bank demand deposits were redeemable in Bank of England
notes, which in turn were redeemable in gold. There was a
double-inverted pyramid in the banking system. At the bottom pyramid,
the Bank of England, engaging in fractional-reserve banking, multiplied
fake warehouse receipts to gold–its notes and deposits–on top of its
gold reserves. In their turn, in a second inverted pyramid on top of the
Bank of England, the private commercial banks pyramided their demand
deposits on top of *their* reserves, or their deposit accounts, at the
Bank of England. It is clear that, once Britain went off the gold
standard, first during World War I and finally in 1931, the Bank of
England notes could serve as the standard fiat money, and the private
banks could still pyramid demand deposits on top of their Bank of
England reserves. The big difference is that now the gold standard no
longer served as any kind of check upon the Central Bank's expansion of
its credit, i.e., its counterfeiting of notes and deposits.


Anzumerken ist auch die Wirkung der Restriktionen auf Privatbanken;
diese konnten weiterhin Obligationen in Form von Bankeinlagen erzeugen,
jedoch nicht in Form von Noten. Zum ersten Mal macht die *Form* der
Obligationen einen großen Unterschied, denn wenn Bankkunden *Noten* d.h.
Bargeld bzw. Papiergeld und nicht immaterielle Einlagen bzw.
Bankguthaben haben wollten, mussten die Banken bei der Zentralbank ihre
Reserven abziehen. Wie wir später bei der Analyse der Federal Reserve
sehen werden, ändert sich die Geldmenge wenn sich in diesem System die
Form der Obligationen ändert. Wenn mehr Einlagen statt Noten gehalten
werden, führt dies zu einer Kontraktion der Geldmenge, während eine
Änderung der Form von Noten zu Einlagen, zu einer Expansion der
Geldmenge führt.

Note, too, that with the prohibition of private bank issue of *notes*,
in contrast to demand deposits, for the first time the *form* of
warehouse receipt, whether notes or deposits, made a big difference. If
bank customers wish to hold cash, or paper notes, instead of intangible
deposits, their banks have to go to the Central Bank and draw down their
reserves. As we shall see later in analyzing the Federal Reserve, the
result is that a change from demand deposit to note has a contractionary
effect on the money supply, whereas a change from note to intangible


