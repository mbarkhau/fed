Inflation und Fälschung von Geld
================================

Monetary Inflation and Counterfeiting
=====================================


Nehmen wir an ein Edelmetall, wie etwa Gold, wird zum Geld einer
Gesellschaft und ein bestimmtes Gewicht an Gold wird zur Einheit des
Geldes, worin alle Preise ausgedrückt werden. Solange die Gesellschaft
diesen Gold- oder Silberstandard beibehält, wird die zusätzliche Menge
an Geld in jedem Jahr durch den Abbau in Goldbergwerke wahrscheinlich
sehr gering sein. Die Gesamtmenge an Gold ist sehr beschränkt und es ist
sehr kostspielig mehr Gold abzubauen; desweiteren hat die große
Haltbarkeit von Gold zur Folge, dass die jährliche Produktion sehr
gering sein wird, verglichen mit der bisher über die Jahrhunderte
angehäufte Menge an Gold. Die Währung wird einen relativ stabilen Wert
beibehalten; in einer wachsenden Wirtschaft wird die erhöhte jährliche
Produktion von Gütern das beständige Wachstum der Geldmenge mehr als
ausgleichen. Daraus resultiert ein ständig sinkendes Preisniveau, eine
jährliche Erhöhung der Kaufkraft von jeder einzelnen Einheit des Geldes
bzw. von jedem Gramm Gold. Das leicht fallende Preisniveau bedeute einen
beständigen Anstieg in der Kaufkraft von dem Dollar oder dem Euro, womit
Sparen und Investition in zukünftige Produktion angeregt wird. Sich
erhöhende Produktion und fallende Preise sind ein Zeichen des steigenden
Wohlstands für jede Person in einer Gesellschaft. Typischerweise fallen
die Lebenskosten stetig, während die nominalen Löhne gleich bleiben, was
gleichbedeutend mit einer Erhöhung der "echten" Löhne ist, bzw. mit
einer jährlichen Erhöhung des Wohlstands von jedem einzelnen Arbeiter.
Wir haben uns mittlerweile derart an ständige Preisinflation gewöhnt,
dass die Idee jährlich *fallender* Preise nur schwer zu begreifen ist.
Dennoch fielen Preise im allgemeinen mit jedem Jahr seit Anfang der
Industriellen Revolution Mitte des achtzehnten Jahrhunderts bis 1940.
Ausgenommen davon waren Kriegszeiten in denen Regierungen anfingen
radikale Inflation zu betreiben und Preise in die Höhe zu treiben,
wonach der Trend fallender Preise sich jedoch wieder fortsetzte. Hierbei
müssen wir beachten, dass fallende Preise nicht gleichbedeutend mit
einer Depression sind, denn die Produktionskosten verringerten sich
aufgrund erhöhter Produktivität ebenfalls, sodass Profite nicht fielen.
Wenn wir uns den spektakulären Preisverfall der letzten Jahre innerhalb
bestimmter Branchen anschauen, wie etwa Computer, Fernseher und
Mobiltelefone, sehen wir, dass fallende Preise keinesfalls eine
Depression implizieren müssen.

Suppose that a precious metal such as gold becomes a society's money,
and a certain weight of gold becomes the currency unit in which all
prices and assets are reckoned. Then, so long as the society remains on
this pure gold or silver "standard," there will probably be
only gradual annual increases in the supply of money, from the output of
gold mines. The supply of gold is severely limited, and it is costly to
mine further gold; and the great durability of gold means that any
annual output will constitute a small portion of the total gold stock
accumulated over the centuries. The currency will remain of roughly
stable value; in a progressing economy, the increased annual production
of goods will more than offset the gradual increase in the money stock.
The result will be a gradual fall in the price level, an increase in the
purchasing power of the currency unit or gold ounce, year after year.
The gently falling price level will mean a steady annual rise in the
purchasing power of the dollar or franc, encouraging the saving of money
and investment in future production. A rising output and falling price
level signifies a steady increase in the standard of living for each
person in society. Typically, the cost of living falls steadily, while
money wage rates remain the same, meaning that "real" wage
rates, or the living standards of every worker, increase steadily year
by year. We are now so conditioned by permanent price inflation that the
idea of prices *falling* every year is difficult to grasp. And
yet, prices generally fell every year from the beginning of the
Industrial Revolution in the latter part of the eighteenth century until
1940, with the exception of periods of major war, when the governments
inflated the money supply radically and drove up prices, after which
they would gradually fall once more. We have to realize that falling
prices did not mean depression, since costs were falling due to
increased productivity, so that profits were not sinking. If we look at
the spectacular drop in prices (in *real* even more than in money
terms) in recent years in such particularly booming fields as computers,
calculators, and TV sets, we can see that falling prices by no means
have to connote depression.


Lasst uns jedoch annehmen, dass innerhalb von diesem Idyll des
Wohlstands starker Währungen und erfolgreicher wirtschaftlicher
Kalkulation, eine Schlange in Eden erscheint: Die Versuchung der
Fälschung, also ein nahezu wertloses Objekt so zu gestalten, dass
Menschen es für echtes Geld verwechseln. Es ist aufschlussreich wenn wir
das resultierende Ergebnis nachvollziehen. Durch Fälschung entsteht
ein Problem, sofern sie "erfolgreich" ist, d.h. sofern die Fälschungen
nicht entlarvt werden.

But let us suppose that in this idyll of prosperity, sound money, and
successful monetary calculation, a serpent appears in Eden: the
temptation to counterfeit, to fashion a near-valueless object so that it
would fool people into thinking it was the money-commodity. It is
instructive to trace the result. Counterfeiting creates a problem to the
extent that it is "successful," i.e., to the extent that the
counterfeit is so well crafted that it is not found out.


Stellen wir uns vor, dass Hinz und Kunz die perfekte Fälschung erfunden
haben: Bei einem Goldstandard wäre das ein Objekt aus Messing oder
Plastik, welches nicht von einer Goldmünze zu unterscheiden ist, oder im
Falle des derzeitigen Papiergelds, ein $10 Schein der eine exakte Kopie
einer $10 Banknote aus der US-Notenbank darstellt. Was würde geschehen?

Suppose that Joe Doakes and his merry men have invented a perfect
counterfeit: under a gold standard, a brass or plastic object that would
look exactly like a gold coin, or, in the present paper money standard,
a $10 bill that exactly simulates a $10 Federal Reserve Note. What would
happen?


Zunächst würde die Gesamtmenge des Geldes im Land um die Menge des
gefälschten Geldes zunehmen; nicht weniger wichtig ist die Tatsache,
dass die Fälscher dieses Geld als *erste* in die Hände bekommen. Kurz
gesagt, die Fälschung hat zwei Folgen: (1) Das absolute Angebot an Geld
wird vergrößert, wodurch die Preise von Waren und Dienstleistungen
hochgetrieben werden und die Kaufkraft jeder einzelnen Einheit des
Geldes sich verringert; und (2) da eine unverhältnismäßig größere Menge
an Geld in den Händen der Fälscher gerät, wird die Verteilung von
Einkommen und Vermögen zu ihrem Gunsten verändert.

In the first place, the aggregate money supply of the country would
increase by the amount counterfeited; equally important, the new money
will appear *first* in the hands of the counterfeiters
themselves. Counterfeiting, in short, involves a twofold process: (1)
increasing the total supply of money, thereby driving up the prices of
goods and services and driving down the purchasing power of the
money-unit; and (2) changing the distribution of income and wealth, by
putting disproportionately more money into the hands of the
counterfeiters.


Mit dem ersten Teil von diesem Ablauf, die Vergrößerung von dem
absoluten Angebot an Geld in einem Land, beschäftigten sich die Briten
der Klassischen Nationalökonomie wie David Hume oder Ricardo. Die
Geldmenge ist der Schwerpunkt ihrer "Quantitätstheorie" und wird heute
noch von Milton Friedman und den Monetaristen der "Chicagoer Schule"
verfolgt. Um die inflationäre und unproduktive Wirkung des Papiergeldes
zu zeigen, benutzte David Hume ein Modell, dass ich auch gerne das
Modell vom "Erzengel Gabriel" nenne. In diesem Modell steigt der
Erzengel Gabriel vom Himmel herab, um die Gebete nach mehr Geld zu
erhören und verdoppelt von heut auf morgen den Geldbestand jeder Person.
(In diesem Fall ist der Erzengel Gabriel der "Fälscher", wenn auch mit
guter Absicht). Es ist klar, dass jeder wegen der scheinbaren
Verdoppelung seines geldlichen Reichtums zwar euphorisch wäre, die
Gesellschaft jedoch in keiner Weise besser dasteht als vorher: Denn es
hat sich weder an dem Kapital, der Produktivität oder dem Angebot an
Güter etwas geändert. Sobald die Menschen herauseilen um ihr neues Geld
auszugeben, wäre die einzige Wirkung eine ungefähre Verdoppelung aller
Preise und eine Halbierung der Kaufkraft von jedem Dollar oder Euro,
ohne irgend einen gesellschaftlichen Nutzen. Eine Vermehrung des Geldes
kann lediglich seine Effektivität verwässern. Milton Friedman hat eine
modernere aber gleichermaßen märchenhafte Version von diesem Modell, den
er als "Helikopter Effekt" bezeichnet. Hierbei sieht er die jährliche
Vermehrung des Geldes durch die US-Notenbank analog zu einem magischen
Helikopter der Regierung, dass über jede Person Geld herabrieseln
lässt, proportional zu seinem derzeitigen Geldbestand.

The first part of the process, increasing the total money supply in the
country, was the focus of the "quantity theory" of the British
classical economists from David Hume to Ricardo, and continues to be the
focus of Milton Friedman and the monetarist "Chicago school."
David Hume, in order to demonstrate the inflationary and non-productive
effect of paper money, in effect postulated what I like to call the
"Angel Gabriel" model, in which the Angel, after hearing pleas
for more money, magically doubled each person's stock of money
overnight. (In this case, the Angel Gabriel would be the
"counterfeiter," albeit for benevolent motives.) It is clear
that while everyone would be euphoric from their seeming doubling of
monetary wealth, society would in no way be better off: for there would
be no increase in capital or productivity or supply of goods. As people
rushed out and spent the new money, the only impact would be an
approximate doubling of all prices, and the purchasing power of the
dollar or franc would be cut in half, with no social benefit being
conferred. An increase of money can only dilute the effectiveness of
each unit of money. Milton Friedman's more modern though equally magical
version is that of his "helicopter effect," in which he
postulates that the annual increase of money created by the Federal
Reserve is showered on each person proportionately to his current money
stock by magical governmental helicopters.


Die Analyse von Hume ist soweit es geht zwar aufschlussreich und
richtig, es übersieht jedoch die wesentliche Folge der Umverteilung. Bei
dem "Helikopter Effekt" von Friedman wird die Analyse erheblich
verzerrt, da es so konstruiert ist, dass jede umverteilende Wirkung von
vorn herein ausgeschlossen wird. Im wesentlichen können wir zwar
annehmen, dass der Erzengel Gabriel gutmütig motiviert ist, die gleiche
Annahme für eine fälschende Regierung oder für die US-Notenbank können
wir jedoch nicht machen. Tatsächlich stellt sich für jeden irdischen
Fälscher die Frage nach dem Sinn von Fälschung, wenn jede Person das
neue Geld anteilig erhält.

While Hume's analysis is perceptive and correct so far as it goes, it
leaves out the vital redistributive effect. Friedman's "helicopter
effect" seriously distorts the analysis by being so constructed
that redistributive effects are ruled out from the very beginning. The
point is that while we can assume benign motives for the Angel Gabriel,
we cannot make the same assumption for the counterfeiting government or
the Federal Reserve. Indeed, for any earthly counterfeiter, it would be
difficult to see the point of counterfeiting if each person is to
receive the new money proportionately.


Demnach ist im echten leben der ganze Sinn der Fälschung ein Prozess in
Gang zu setzen, ein Prozess bei dem neues Geld von einer Tasche in eine
andere wandert und nicht das Ergebnis einer magischen, proportionalen
und gleichzeitigen Ausweitung der Geldmenge in den Taschen aller
Personen. Ob Fälschung durch die Herstellung von Münzen aus Messing oder
Plastik um Gold zu emulieren geschieht, oder durch bedruckte
Papierscheine die denen der Regierung gleichen, bei der Fälschung ist es
immer der Fälscher der das neue Geld als erster erhält. Dieser Vorgang
wurde in einer alten Karikatur des *New Yorker* verzeichnet, bei dem
eine Fälschergruppe zuschaut wie ihr erster $10 Schein aus der
Druckerpresse herauskommt. Einer äußert dabei: "Junge, jetzt können wir
hier in der Gegend die *Einzelhandelsausgaben* ankurbeln!"

In real life, then, the very point of counterfeiting is to constitute a
process, a process of transmitting new money from one pocket to another,
and not the result of a magical and equi-proportionate expansion of
money in everyone's pocket simultaneously. Whether counterfeiting is in
the form of making brass or plastic coins that simulate gold, or of
printing paper money to look like that of the government, counterfeiting
is always a process in which the counterfeiter gets the new money first.
This process was encapsulated in an old *New Yorker* cartoon, in
which a group of counterfeiters are watching the first $10 bill emerge
from their home printing press. One remarks: "Boy, is retail
spending in the neighborhood in for a shot in the arm!"


Und das konnten sie tatsächlich. Die ersten die das neue Geld bekommen
sind die Fälscher, die es dann benutzen um diverse Waren und
Dienstleistungen zu erwerben. Die zweiten die das Geld bekommen sind die
Händler die diese Güter an die Fälscher verkaufen. So sickert das neue
Geld weiter durch das System von einer Tasche in die nächste. Bei diesem
Vorgang geschieht die Umverteilung sehr schnell. Zunächst die Fälscher,
dann die Händler, usw. bekommen neues Geld und Einkommen womit sie auf
Waren und Dienstleistungen bieten, die Nachfrage erhöhen und damit auch
die Preise der von ihnen gekauften Güter hochtreiben. Während die Preise
anfangen als Reaktion auf die größere Menge an Geld zu steigen, müssen
diejenigen die kein neues Geld erhalten haben feststellen, dass die
Preis der von ihnen gekauften Güter zwar gestiegen sind, die
Verkaufspreise für ihre eigenen Güter bzw. ihre Einkommen jedoch nicht
gestiegen sind. Kurz gesagt, diejenigen die bei diesem Ablauf das neue
Geld als erste erhalten, profitieren auf kosten derjenigen die das Geld
erst später erhalten und umso mehr Verlierern solche Personen (wie etwa
solche mit festem Einkommen aus Pensionen, Zinsen oder Renten) die das
Geld niemals erhalten. Die Inflation des Geldes hat demnach die Wirkung
einer versteckten "Steuer", wodurch Personen die das Geld früher
bekommen diejenigen enteignen (d.h. auf ihre Kosten profitieren) die es
später bekommen. Derjenige der das Geld als erster erhält ist freilich
der Fälscher selbst und demnach profitiert er am meisten. Diese Steuer
ist besonders hinterlistig, da sie in dem Geld- und Bankenwesen
versteckt ist, von dem nur wenige ein gutes Verständnis haben und weil
es nur allzu leicht ist die Schuld auf steigende Preise zu schieben,
also die von der Inflation des Geldes erzeugte "Preisinflation" irgend
einer leicht zu denunzierenden Minderheit anzulasten, sei es gierige
Kapitalisten, Spekulanten oder ausgabefreudige Konsumenten. Es ist
offensichtlich auch im Interesse der Fälscher von ihrer eigenen
wesentlichen Rolle abzulenken, indem sie jede erdenkliche soziale Gruppe
und Institution als für die Preisinflation verantwortlich denunzieren.

And indeed it was. The first people who get the new money are the
counterfeiters, which they then use to buy various goods and services.
The second receivers of the new money are the retailers who sell those
goods to the counterfeiters. And on and on the new money ripples out
through the system, going from one pocket or till to another. As it does
so, there is an immediate redistribution effect. For first the
counterfeiters, then the retailers, etc., have new money and monetary
income which they use to bid up goods and services, increasing their
demand and raising the prices of the goods that they purchase. But as
prices of goods begin to rise in response to the higher quantity of
money, those who haven't yet received the new money find the prices of
the goods they buy have gone up, while their own selling prices or
incomes have not risen. In short, the early receivers of the new money
in this market chain of events gain at the expense of those who receive
the money toward the end of the chain, and still worse losers are the
people (e.g., those on fixed incomes such as annuities, interest, or
pensions) who never receive the new money at all. Monetary inflation,
then, acts as a hidden "tax" by which the early receivers
expropriate (i.e., gain at the expense of) the late receivers. And of
course since the very earliest receiver of the new money is the
counterfeiter, the counterfeiter's gain is the greatest. This tax is
particularly insidious because it is hidden, because few people
understand the processes of money and banking, and because it is all too
easy to blame the rising prices, or "price inflation," caused
by the monetary inflation on greedy capitalists, speculators,
wild-spending consumers, or whatever social group is the easiest to
denigrate. Obviously, too, it is to the interest of the counterfeiters
to distract attention from their own crucial role by denouncing any and
all other groups and institutions as responsible for the price
inflation.


Besonders heimtückisch und zerstörerisch an dem Ablauf der Inflation
ist, dass jeder einzelne sich zwar freut mehr Geld zu haben, sich alle
jedoch über die Folgen beschweren wenn es tatsächlich mehr Geld gibt,
nämlich die höheren Preise. Da es jedoch eine unumgängliche Verzögerung
zwischen dem Anstieg der Geldmenge gibt und dem daraus folgenden Anstieg
der Preise, und da die Öffentlichkeit wenig Wissen über die Ökonomie des
Geldes hat, ist es nur all zu leicht ihre Schuldzuweisung eher auf
sichtbare Gruppen zu lenken als auf die Fälscher.

The inflation process is particularly insidious and destructive because
everyone enjoys the feeling of having more money, while they generally
complain about the consequences of more money, namely higher prices. But
since there is an inevitable time lag between the stock of money
increasing and its consequence in rising prices, and since the public
has little knowledge of monetary economics, it is all too easy to fool
it into placing the blame on shoulders far more visible than those of
the counterfeiters.


Der große Fehler aller Anhänger der Quantitätstheorie, seien es die
Briten der Klassik oder Milton Friedman, ist anzunehmen, dass Geld
lediglich ein "Schleier" ist, und dass eine Erhöhung der Geldmenge sich
lediglich auf das Preisniveau oder auf die Kaufkraft einer Einheit des
Geldes auswirkt. Im Gegenteil, es ist eines der beachtenswerten Beiträge
von Ökonomen der "Österreichischen Schule" und ihre Vorgänger wie der
Irisch-Französische Ökonom Richard Cantillon, der im frühen achtzehnten
Jahrhundert nicht nur auf die Gesamtwirkung einer Vermehrung des Geldes
hinwies, sondern auch auf die Änderungen der Verteilung von Einkommen
und Vermögen. Die langsame Ausbreitung ändert auch die Struktur der
Preise im Verhältnis zueinander und demnach auch die Arten und Mengen
der Güter die produziert werden. Diese Veränderungen richten sich nach
den Vorlieben der Fälscher und anderer die das Geld früher erhalten und
haben dadurch die Wirkung einer "Besteuerung" derjenigen die das Geld
später erhalten, zugunsten derer die das Geld früher erhalten.
Desweiteren, sind diese Änderungen der Verteilung von Einkommen,
Ausgaben, relative Preise, und Produktion etwas permanentes und sie
verschwinden nicht einfach wie es die Anhänger der Quantitätstheorie
ungeniert annehmen, sobald sich die Auswirkungen der Vermehrung des
Geldes ausgebreitet haben.

The big error of all quantity theorists, from the British classicists to
Milton Freidman, is to assume that money is only a "veil," and
that increases in the quantity of money only have influence on the price
level, or on the purchasing power of the money unit. On the contrary, it
is one of the notable contributions of "Austrian School"
economists and their predecessors, such as the early-eighteenth-century
Irish-French economist Richard Cantillon, that, in addition to this
quantitative, aggregative effect, an increase in the money supply also
changes the distribution of income and wealth. The ripple effect also
alters the structure of relative prices, and therefore of the kinds and
quantities of goods that will be produced, since the counterfeiters and
other early receivers will have different preferences and spending
patterns from the late receivers who are "taxed" by the
earlier receivers. Furthermore, these changes of income distribution,
spending, relative prices, and production will be permanent and will not
simply disappear, as the quantity theorists blithely assume, when the
effects of the increase in the money supply will have worked themselves
out.


Die Erkenntnis der Österreichischen Schule ist zusammengefasst, dass
Fälschung weit schwerwiegendere Folgen für die Wirtschaft hat als
einfach nur eine Inflation des Preisniveaus. Sie verursacht andere
permanente Verzerrungen in der Wirtschaft, weg von der Struktur des
freien Marktes, welches auf die Nachfrage von Konsumenten und
Eigentümern in der freien Wirtschaft reagiert. Nun kommen wir zu einem
wichtigen Aspekt der Fälschung den wir nicht übersehen dürfen.
Zusätzlich zu den eher beschränkten wirtschaftlichen Verzerrungen und
seine bedauerlichen Folgen, verkrüppelt Fälschung von Geld das
moralische Fundament sowie die Eigentumsrechte die jeder freien
Marktwirtschaft zugrundeliegen.

In sum, the Austrian insight holds that counterfeiting will have far
more unfortunate consequences for the economy than simple inflation of
the price level. There will be other, and permanent, distortions of the
economy away from the free market pattern that responds to consumers and
property-rights holders in the free economy. This brings us to an
important aspect of counterfeiting which should not be overlooked. In
addition to its more narrowly economic distortion and unfortunate
consequences, counterfeiting gravely cripples the moral and property
rights foundation that lies at the base of any free-market economy.


Betrachten wir vor diesem Hintergrund den freien Markt einer
Gesellschaft die Gold als Geld benutzt. In solch einer Gesellschaft ist
es auf dreierlei Art möglich Geld anzueignen: (a) indem man es in einer
Mine abbaut; (b) indem man eine Ware oder Dienstleistung gegen das Gold
eines anderen austauscht; oder (c) indem man das Gold durch freiwillige
Schenkung von einem anderen Goldbesitzer erhält. Bei all diesen
Vorgängen werden die Rechte von jedem an sein Privateigentum respektiert
und verteidigt. Sagen wir, dass nun ein Fälscher erscheint. Indem er
gefälschte Goldmünzen herstellt, kann er durch Betrug zum Eigentümer von
Geld werden, womit er auf dem Markt Ressourcen erlangt, indem er die
rechtmäßige Eigentümer von Gold überbietet. So beraubt er die
derzeitigen Eigentümer von Gold ebenso sicher und noch umfassender, als
wenn er ihre Häuser oder Tresore geplündert hätte. Denn auf diese Art,
kann er ohne tatsächlich auf anderer Personen Grundstücke einzubrechen,
sie hinterlistig die Früchte ihrer Arbeit berauben, und zwar auf Kosten
aller andere Geldbesitzer, insbesonderen derjenigen die das neue Geld
als letzte erhalten.

Thus, consider a free-market society where gold is the money. In such a
society, one can acquire money in only three ways: (a) by mining more
gold; (b) by selling a good or service in exchange for gold owned by
someone else; or (c) by receiving the gold as a voluntary gift or
bequest from some other owner of gold. Each of these methods operates
within a principle of strict defense of everyone's right to his private
property. But say a counterfeiter appears on the scene. By creating fake
gold coins he is able to acquire money in a fraudulent and coercive way,
and with which he can enter the market to bid resources away from
legitimate owners of gold. In that way, he robs current owners of gold
just as surely, and even more massively, than if he burglarized their
homes or safes. For this way, without actually breaking and entering the
property of others, he can insidiously steal the fruits of their
productive labor, and do so at the expense of all holders of money, and
especially the later receivers of the new money.


Fälschung ist deshalb inflationär, umverteilend, es verzerrt das
Wirtschaftssystem und ist im Wesentlichen ein heimlicher und
heimtückischer Diebstahl und eine Enteignung aller legitimen Eigentümer
der Gesellschaft.

Counterfeiting, therefore, is inflationary, redistributive, distorts the
economic system, and amounts to stealthy and insidious robbery and
expropriation of all legitimate property-owners in society.

