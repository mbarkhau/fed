Der New Deal und die Ablösung von Morgan
========================================

The New Deal and the Displacement of the Morgans
================================================


Benjamin Strong war nicht die einzige Person der Morgans, die während
den 1920er für ihre Dominanz über die Politik und das Finanzwesen von
Amerika sorgte. Präsident Calvin Coolidge, der die Nachfolge des
Rockefeller alliierten Präsident Harding antrat, als dieser im Amt
starb, war ein enger persönlicher Freund von J. P. Morgan junior.
Darüber hinaus war Coolidge ein politischer Zögling der Morgan Partner,
Dwight Morrow, einem ehemaligen Klassenkameraden auf dem Amherst
College, sowie von Thomas Cochran. Während den Amtsperioden der
Republikaner in den 1920er, diente Andrew W. Mellon als Finanzminister.
Mellon war ein Multimillionär und Großindustrieller aus Pitsburg, sowie
Interessenvertreter der Mellons, die langjährige Alliierte mit den
Morgans waren. Der spätere Präsident, Herbert Hoover, war zwar nicht
annähernd so eng mit den Morgans verbunden wie etwa Coolidge, stand aber
schon lange den Morgan Interessen nahe. Ogden L. Mills, der Mellon als
Finanzminister im Jahre 1931 ersetzte und Hoover nahe stand, war der
Sohn von Ogden Mills, der führende Positionen in den
Eisenbahngesellschaften von Morgan einnahm, wie etwa der New York
Central Railroad. In der Zwischenzeit wurde Henry L. Stimson von Hoover
zum Außenminister der Vereinigten Staaten ernannt. Stimson war einen
prominenten Schüler und juristischer Partner von Elihu Root, dem
ehemaligen persönlichen Anwalt von Morgan. Noch aufschlussreicher sind
zwei inoffiziellen jedoch mächtigen Berater von Hoover in seiner
Amtszeit; zum einen Thomas W. Lamont, dem Nachfolger von Davision als
Geschäftsführer im Morgan Imperium, sowie dem Morgan Partner, Dwight
Morrow, mit dem sich Hoover drei Mal die Woche regulär beriet.

It was not only through Benjamin Strong that the Morgans totally
dominated American politics and finance during the 1920s. President
Calvin Coolidge, who succeeded Rockefeller ally President Harding when
he died in office, was a close personal friend of J. P. Morgan, Jr., and
a political protege of Coolidge's Amherst College classmate, Morgan
partner Dwight Morrow, as well as of fellow Morgan partner Thomas
Cochran. And throughout the Republican administrations of the 1920s, the
Secretary of the Treasury was multimillionaire Pittsburgh tycoon Andrew
W. Mellon, whose Mellon interests were long-time allies of the Morgans.
And while President Herbert Hoover was not nearly as intimately
connected to the Morgans as Coolidge, he had long been close to the
Morgan interests. Ogden Mills, who replaced Mellon as Treasury Secretary
in 1931 and was close to Hoover, was the son of a leader in such Morgan
railroads as New York Central; in the meanwhile, Hoover chose as
Secretary of State Henry L. Stimson, a prominent disciple and law
partner of Morgan's one-time personal attorney, Elihu Root. More
tellingly, two unofficial but powerful Hoover advisers during his
administration were Morgan partners Thomas W. Lamont (the successor to
Davison as Morgan Empire CEO) and Dwight Morrow, whom Hoover regularly
consulted three time a week.


Ein wesentlicher Aspekt der ersten Amtszeit von Roosevelt und seinem New
Deal, ist leider von regulären Historikern vernachlässigt worden: Der
New Deal stellte eine gezielte Abstufung und Ablösung der Dominanz von
Morgan dar. Eine Koalition oppositioneller und finanzieller
Interessengruppen, sorgten im New Deal für den Sturz der Macht von
Morgan. Diese Koalition bestand aus den Rockefellers; die aufkeimende
Macht von Harriman in der Demokratischen Partei; neue und dreiste
Jüdische Investmentbanken auf Wall Street, wie etwa die Lehman Brothers
und Goldman Sachs, die Kuhn, Loeb verdrängten; ethnische Randgruppen zu
denen etwa der irisch katholische Freibeuter Joseph P. Kennedy gehörte,
oder italienisch-amerikanische Familien wie die Gianninis der
kalifornischen Bank of Amerika; und nicht zuletzt, Mormonen wie Marriner
Eccles aus Utah, dem Kopf eines Großkonzerns im Banken-, Holding- und
Baugewerbe, der darüber hinaus mit dem in Kalifornien stationierten
Bauunternehmen, dem Bechtel Corporation, sowie dem Rockefeller
Unternehmen, Standard Oil of California, alliiert war.

A crucial aspect of the first term of the Roosevelt New Deal, however,
has been sadly neglected by conventional historians: The New Deal
constituted a concerted Bringing Down and displacement of Morgan
dominance, a coalition of opposition financial out-groups combined in
the New Deal to topple it from power. This coalition was an alliance of
the Rockefellers; a newly-burgeoning Harriman power in the Democratic
Party; newer and brasher Wall Street Jewish investment banks such as
Lehman Brothers and Goldman Sachs pushing Kuhn, Loeb into the shade; and
such ethnic out-groups as Irish Catholic buccaneer Joseph P. Kennedy,
Italian-Americans such as the Giannini family of California's Bank of
America, and Mormons such as Marriner Eccles, head of a vast Utah
banking-holding company-construction conglomerate, and allied to the
California-based Bechtel Corporation in construction and to the
Rockefeller's Standard Oil of California.


Der wesentliche Vorbote dieser Revolution im Finanzwesen, war die
erfolgreiche Übernahme der größten kommerziellen Bank von Morgan durch
Rockefeller, der gewaltigen Chase National Bank of New York. Winthrop W.
Aldrich, der Sohn von Senator Nelson Aldrich und Schwager von John D.
Rockefeller junior, fädelte nach der Krise von 1929 die Firmenfusion
zwischen der von Rockefeller kontrollierten Equitable Trust Company und
der Chase Bank ein. Ab dem Zeitpunkt konzentrierte sich Aldrich auf den
titanischen Kampf mit Chase, in dem er es bis 1932 schaffte den Morgan
verbundenen Firmenchef von Chase, Albert Wiggen, zu vertreiben und
selbst an seiner Stelle zu treten. Seitdem ist Chase praktisch zum
allgemeinen Hauptquartier des Finanzimperiums von Rockefeller geworden.

The main harbinger of this financial revolution was the Rockefeller's
successful takeover of the Morgan's flagship commercial bank, the mighty
Chase National Bank of New York. After the 1929 crash, Winthrop W.
Aldrich, son of Senator Nelson Aldrich and brother-in-law ofJohn D.
Rockefeller, Jr., engineered a merger of his Rockefeller-controlled
Equitable Trust Company into Chase Bank. From that point on, Aldrich
engaged in a titanic struggle within Chase, by 1932 managing to oust the
Morgan's Chase CEO Albert Wiggin and to replace him by Aldrich himself.
Ever since, Chase has been the virtual general headquarters of the
Rockefeller financial Empire.


Die listige neue Koalition schaffte es in dem New Deal, mit Gesetzen aus
den Jahren 1933 und 1935, neue Regulierungen des Bankenwesens zu
verabschieden, mit denen sich die Gestalt der Fed grundsätzlich änderte.
Die wesentliche Macht innerhalb der Fed wurde weg von Wall Street,
Morgan und der New York Fed geschafft, und hin zu den Politikern in
Washington D.C. verschoben. Das Ergebnis dieser beiden Gesetze zum
Bankenwesen war, die New York Fed ihrer Macht in der Offenmarktpolitik
zu berauben, und diese Macht ganz und gar in den Händen des Federal Open
Market Committee zu legen, die von dem Gremium in Washington dominiert
wurde, wobei die regionalen Privatbanken die Rolle untergeordneter
Partner annehmen sollten.[^40]

The new coalition cunningly drove through the New Deal's Banking Acts of
1933 and 1935, which transformed the face of the Fed, and permanently
shifted the crucial power in the Fed from Wall Street, Morgan, and the
New York Fed, to the politicos in Washington, D.C. The result of these
two Banking Acts was to strip the New York Fed of power to conduct
open-market operations, and to place it squarely in the hands of the
Federal Open Market Committee, dominated by the Board in Washington, but
with regional private bankers playing a subsidiary partnership role.[^40]


[^40]: Siehe insb. Thomas Ferguson: "Industrial Conflict and the
    Coming of the New Deal: the Triumph of Multinational Liberalism in
    America," in *The Rise and Fall of the New Deal Order, 1930–1980*, 
    Steve Fraser and Gary Gerstle, eds. (Princeton: Princeton 
    University Press, 1989), S. 3–31. Siehe auch den längeren Artikel
    von Ferguson: "From Normalcy to New Deal: Industrial Structure, 
    Party Competition, and American Public Policy in the Great 
    Depression," *Industrial Organization* 38, no. 1 (Winter 1984).

[^40]: See, in particular, Thomas Ferguson, "Industrial Conflict and the
    Coming of the New Deal: the Triumph of Multinational Liberalism in
    America," in *The Rise and Fall of the New Deal Order, 1930–1980*
    , Steve Fraser and Gary Gerstle, eds. (Princeton: Princeton 
    University Press, 1989), pp. 3–31. Also see the longer article by
    Ferguson, "From Normalcy to New Deal: Industrial Structure, Party
    Competition, and American Public Policy in the Great Depression,"
    *Industrial Organization* 38, no. 1 (Winter 1984).


Die andere im New Deal durchgesetzte große Änderung am Währungssystem,
war natürlich die Lösung von der Golddeckung; eine unter dem Deckmantel
eines "Notfalls" in dem Teilreservensystem der Banken durchgeführte
Maßnahme, ausgelöst durch die Weltwirtschaftskrise. Nach 1933, konnten
Amerikaner die Noten der Federal Reseve, sowie ihre Bankeinlagen, nicht
länger für Goldmünzen einlösen; und nach 1971, konnten auch ausländische
Regierungen und Zentralbanken nicht länger ihre Dollar gegen Goldbarren
einlösen. Das Gold der Amerikaner wurde konfisziert und gegen Federal
Reserve Noten getauscht, die danach zum gesetzlichen Zahlungsmittel
wurden. So kam es, dass Amerikaner sich mit einer deckungslosen
Währungsordnung abfanden, bestehend aus vom Staat und der Federal
Reserve herausgegebenes Fiatgeld, bzw. Papiergeld mit praktisch keinem
intrinsischen Wert. Mit den Jahren sind alle Einschränkungen auf die
Aktivitäten der Fed, sowie ihre Herausgabe von Krediten, völlig
aufgelöst worden; tatsächlich hat die Federal Reserve seit 1980,
buchstäblich *unbeschränkte* Macht: Es kann nicht nur Staatsanleihen der
Vereinigten Staaten kaufen, sonder jedweder und so viele Anlagen die es
möchte. Es ist ihr mittlerweile möglich, ohne Beschränkungen und
beliebig viel Inflation mit Krediten zu betreiben. Die Fed ist der Herr
über alles in ihrem Blick.

The other major monetary change accomplished by the New Deal, of course,
and done under cover of a depression "emergency" in the fractional
reserve banking system, was to go off the gold standard. After 1933,
Federal Reserve Notes and deposits were no longer redeemable in gold
coins to Americans; and after 1971, the dollar was no longer redeemable
in gold bullion to foreign governments and central banks. The gold of
Americans was confiscated and exchanged for Federal Reserve Notes, which
became legal tender; and Americans were stuck in a regime of fiat paper
issued by the government and the Federal Reserve. Over the years, all
early restraints on Fed activities or its issuing of credit have been
lifted; indeed, since 1980, the Federal Reserve has enjoyed the absolute
power to do literally *anything* it wants: to buy not only U.S.
government securities but any asset whatever, and to buy as many assets
and to inflate credit as much as it pleases. There are no restraints
left on the Federal Reserve. The Fed is the master of all it surveys.


Bei der Betrachtung der im New Deal geschaffenen Änderungen, sollten wir
jedoch davon absehen den Morgans Mitleid zu schenken. Sie wurden zwar in
der ersten Hälfte des New Deals permanent entthront, und sollten auch
nie wieder an die Macht gelangen, die Morgans konnten aber dennoch bis
zum Ende der 1930er, wenn auch gezüchtigt, ihren Platz in der neuen
regierenden Koalition des New Deals einnehmen. Dort spielten sie eine
wichtige Rolle in dem Bestreben der Macht-Elite, in den Zweiten
Weltkrieg einzutreten, insbesondere in Europa, und wiedereinmal auf
Seiten der Briten und Franzosen. Während des Zweiten Weltkrieges,
spielten sie hinter den Kulissen eine entscheidende Rolle bei der
Ausarbeitung des Bretton Woods Abkommens. Zusammen mit Keynes und den
Briten wurde ein Abkommen erstellt, den die Regierung der Vereinigten
Staaten als *fait accompli* der versammelten "freien Welt" in Bretton
Woods am Ende des Krieges präsentierte.[^41]

In surveying the changes wrought by the New Deal, however, we should
refrain from crying for the Morgans. While permanently dethroned by the
first term of the New Deal and never returned to power, the Morgans were
able to take their place, though chastened, in the ruling New Deal
coalition by the end of the 1930s. There, they played an important role
in the drive by the power elite to enter World War II, particularly the
war in Europe, once again on the side of Britain and France. During
World War II, furthermore, the Morgans played a decisive
behind-the-scenes role in hammering out the Bretton Woods Agreement with
Keynes and the British, an agreement which the U.S. government presented
as a *fait accompli* to the assembled "free world" at Bretton Woods by
the end of the war.[^41]


[^41]: Siehe G. William Domhoff: *The Power Elite and the State: How Policy
    is Made in America* (New York: Aldine de Gruyter, 1990), S. 113–41;
    159–81.

[^41]: See G. William Domhoff, *The Power Elite and the State: How Policy
    is Made in America* (New York: Aldine de Gruyter, 1990), pp. 113–41;
    159–81.


Tatsächlich befinden sich die verschiedenen finanziellen Interessen,
seit dem Zweiten Weltkrieg, in einer neuen permanenten Anordnung: Morgan
und die anderen finanziellen Gruppen haben ihren Platz eingenommen, als
gefügige Teilhaber an der unangefochten von Rockefeller geführten Wall
Street Elite. Seitdem, haben diese Gruppen gemeinsam für einen großen
Teil der Herrscher über das Federal Reserve System gesorgt. So im Falle
von dem Vorsitzenden der Fed, Alan Greenspan, der vor seinem Aufstieg
zum Thron, ein Mitglied des Exekutivkomitees einer führenden Morgan Bank
war, der Morgan Guaranty Trust Company. Sein hoch gepriesener Vorgänger,
der charismatische Paul Volcker, war ein langjähriger prominenter Diener
des Rockefeller Imperiums; zunächst als Ökonom für die Exxon Corporation
von Rockefeller und später in ihrem Hauptquartier, der Chase Manhattan
Bank. (In einer symbolisch wichtigen Firmenfusion wurde die Bank of
Manhattan und damit eine der wichtigsten Banken von Kuhn, Loeb, von
Chase absorbiert.) Es war tatsächlich ein Neue Welt, wenn auch keine
sonderlich schöne. Es sollte für die Eliten von Wall Street zwar noch
viele Herausforderungen geben, sowohl finanzieller Natur als auch in
Form der politischen Macht dreister Neulinge, sowie freibeuterischer
Unternehmer aus Texas und Kalifornien; dennoch hatten sich die
alteingesessenen Interessen aus dem Nordosten, ein harmonisch und
solides Bündnis unter der Führung von Rockefeller geschaffen.

Since World War II, indeed, the various financial interests have entered
into a permanent realignment: the Morgans and the other financial groups
have taken their place as compliant junior partners in a powerful
"Eastern Establishment," led unchallenged by the Rockefellers. Since
then, these groups, working in tandem, have contributed rulers to the
Federal Reserve System. Thus, the present Fed Chairman, Alan Greenspan,
was, before his accession to the throne a member of the executive
committee of the Morgans' flagship commercial bank, Morgan Guaranty
Trust Company. His widely revered predecessor as Fed Chairman, the
charismatic Paul Volcker, was a long-time prominent servitor of the
Rockefeller Empire, having been an economist for the Rocke-fellers'
Exxon Corporation, and for their headquarters institution, the Chase
Manhattan Bank. (In a symbolically important merger, Chase had absorbed
Kuhn, Loeb's flagship commercial bank, the Bank of Manhattan.) It was
indeed a New World, if not a particularly brave one; while there were
still to be many challenges to Eastern Establishment financial and
political power by brash newcomers and takeover buccaneers from Texas
and California, the old-line Northeastern interests had themselves
become harmoniously solidified under Rockefeller rule.

