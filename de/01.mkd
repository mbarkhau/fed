Einführung: Geld und Politik
============================

Introduction: Money and Politics
================================

Die geheimste und eigenmächtigste Behörde der US-Regierung, ist nicht
wie man es erwarten würde, die CIA, die NSA oder dergleichen streng
geheimer Nachrichtendienst. Diese Behörden unterstehen dem US-Kongress.
Ein Kongress-Ausschuss betreut sie, kontrolliert ihre Haushalte und ist
über ihre verdeckten Aktivitäten informiert. Es stimmt zwar, dass die
Anhörungen und Handlungen des Komitees hinter verschlossenen Türen
stattfinden; aber zumindest versichern die vom Volk gewählten
Repräsentanten einen gewissen Grad an Verantwortung für diese geheimen
Behörden.

By far the most secret and least accountable operation of the 
federal government is not, as one might expect, the CIA, DIA, or some
other super-secret intelligence agency. The CIA and other intelligence
operations are under control of the Congress. They are accountable: a
Congressional committee supervises these operations, controls their
budgets, and is informed of their covert activities. It is true that the
committee hearings and activities are closed to the public; but at least
the people's representatives in Congress insure some accountability for
these secret agencies.


Weniger bekannt ist allerdings, dass es eine US-Behörde gibt, die in
Verborgenheit alle anderen Behörden bei weitem übertrifft. Die
US-Notenbank (Federal Reserve oder kurz Fed genannt) ist niemandem
Rechenschaft schuldig; sie hat keinen Haushalt; sie unterliegt keiner
Prüfung; und der gesamte US-Kongress ist weder über ihre Handlungen
informiert, noch kann er sie wirklich leiten. Die US-Notenbank hat
praktisch die komplette Kontrolle über das lebenswichtige nationale
Geldsystem, ist niemandem verantwortlich—und diese merkwürdige
Situation, sofern man sie überhaupt anerkennt, wird unweigerlich sogar
als Tugend angepriesen.

It is little known, however, that there is a federal agency that
tops the others in secrecy by a country mile. The Federal Reserve System
is accountable to no one; it has no budget; it is subject to no audit;
and no Congressional committee knows of, or can truly supervise, its
operations. The Federal Reserve, virtually in total control of the
nation's vital monetary system, is accountable to nobody—and this
strange situation, if acknowledged at all, is invariably trumpeted as a
virtue.


Alls also der erste Demokrat seit über einem Jahrzehnt 1993 zum
Präsidenten gewählt wurde, war der eigenbrötlerische, ehrwürdige
Demokrat und Vorsitzende des Ausschusses zur Bankenregulierung, Henry B.
Gonzalez aus Texas, sehr optimistisch beim Vorschlag einiger seiner
Lieblingsprojekte. Er hatte relativ bescheidene Ziele; er wollte keine
vollkommene Kontrolle über das Budget der Notenbank, sondern lediglich
eine öffentlich Überwachung ihrer Tätigkeiten. Der Gesetzesentwurf von
Gonzalez verlangte die vollständige und unabhängige Prüfungen aller
Tätigkeiten der Notenbank, die Aufzeichnung ihrer Konferenzen, sowie 
die Veröffentlichung der Protokolle dieser Konferenzen nach einer
Woche, anstatt wie derzeit lediglich eine wage Zusammenfassung nach
sechs Wochen. Zusätzlich sollten die Präsidenten der zwölf regionalen
Notenbanken durch den Präsident der Vereinigten Staaten bestimmt 
werden, anstatt wie heute durch die kommerziellen Banken in ihrer 
jeweiligen Region.

Thus, when the first Democratic president in over a decade was
inaugurated in 1993, the maverick and venerable Democratic Chairman of
the House Banking Committee, Texan Henry B. Gonzalez, optimistically
introduced some of his favorite projects for opening up the Fed to
public scrutiny. His proposals seemed mild; he did not call for
full-fledged Congressional control of the Fed's budget. The Gonzalez
bill required full independent audits of the Fed's operations;
videotaping the meetings of the Fed's policy-making committee; and
releasing detailed minutes of the policy meetings within a week, rather
than the Fed being allowed, as it is now, to issue vague summaries of
its decisions six weeks later. In addition, the presidents of the twelve
regional Federal Reserve Banks would be chosen by the president of the
United States rather than, as they are now, by the commercial banks of
the respective regions.


Wie erwartet, kam vom Vorsitzenden der Notenbank, Alan Greenspan,
ein erheblicher Widerstand gegenüber alle solche Vorschläge. Es liegt
schließlich in der Natur eines jeden Bürokraten sich gegen die
Einschränkung seiner ungezügelten Macht zur Wehr zu setzen. Etwas
überraschender war hingegen der Widerstand durch Präsident Clinton,
schließlich würden diese Maßnahmen seine Macht ausweiten. Die Reformen
von Gonzalez, so der Präsident, "laufen Gefahr das Vertrauen der Märkte
in die Notenbank zu untergraben".

It was to be expected that Fed Chairman Alan Greenspan would
strongly resist any such proposals. After all, it is in the nature of
bureaucrats to resist any encroachment on their unbridled power.
Seemingly more surprising was the rejection of the Gonzalez plan by
President Clinton, whose power, after all, would be enhanced by the
measure. The Gonzalez reforms, the President declared, "run the risk of
undermining market confidence in the Fed."


Für sich betrachtet ist die Reaktion des Präsidenten, wenn auch ähnlich
dem seiner Vorgänger, etwas verwirrend. Ist eine Demokratie schließlich
nicht davon abhängig, dass seine Bürger darüber Bescheid wissen, was
sich in der Regierung abspielt, die sie ja wählen müssen? Würden die
öffentliche Protokolle und Prüfungen das Vertrauen der amerikanischen
Öffentlichkeit in ihre Währungsinstitution nicht sogar stärken? Inwiefern
könnten Prüfungen "das Vertrauen der Märkte untergraben"? Warum hängt
das "Vertrauen der Märkte" davon ab, weniger öffentlich bekannt zu
geben, als es etwa bei den Hütern von Militärgeheimnissen der Fall ist,
die ja schließlich von ausländischen Feinden mißbraucht werden könnten?
Was geht hier vor?

On the face of it, this presidential reaction, though traditional
among chief executives, is rather puzzling. After all, doesn't a
democracy depend upon the right of the people to know what is going on
in the government for which they must vote? Wouldn't knowledge and full
disclosure strengthen the faith of the American public in their monetary
authorities? Why should public knowledge "undermine market confidence"?
Why does "market confidence" depend on assuring far less public scrutiny
than is accorded keepers of military secrets that might benefit foreign
enemies? What is going on here?


Die übliche Antwort der Fed und ihrer Gefolgsleute ist, dass jede solche
Maßnahme, egal wie unbedeutend, ihre "Unabhängigkeit von der Politik"
einschränken würde. Dies wird beschworen als offensichtliche und
absolute Wahrheit, und da das Währungssystem sehr wichtig ist, behaupten
sie die Notenbank müsse ganz und gar unabhängig sein.

The standard reply of the Fed and its partisans is that any such
measures, however marginal, would encroach on the Fed's "independence
from politics/' which is invoked as a kind of self-evident absolute. The
monetary system is highly important, it is claimed, and therefore the
Fed must enjoy absolute independence.


"Unabhängigkeit von der Politik" ist natürlich etwas gutes, hört sich
schön an und ist eines der wesentlichen Argumente für unzählige
bürokratische Eingriffe, die seit dem Aufkommen des Progressivismus
vorgebracht wurden. Straßenkehrer, Kontrolle von Häfen, Regulierung der
Industrie, Sozialversicherung, diese und viele weitere Funktionen die
von der Regierung übernommen wurden, sind angeblich "zu wichtig", als
dass man sie politischer Willkür aussetzen dürfte. Es ist schön und gut
private Aktivitäten oder Märkte möglichst frei von staatlicher Kontrolle
und in dem Sinne "unabhängig von der Politik" zu lassen, hier reden wir
allerdings von Institutionen und Unternehmungen des *Staates*. Die
Aussage der *Staat* müsse "unabhängig von der Politik" sein impliziert
etwas ganz anderes, denn anders als die private Industrie und Märkte,
muss die Regierung sich nicht gegenüber Eigentümer oder Konsumenten
rechtfertigen.  Die Regierung kann sich immer nur gegenüber der
Öffentlichkeit und seine Repräsentanten rechtfertigen, und wenn sie
"unabhängig von der Politik" wird kann das nur bedeuten, dass dieser
Teil der Regierung zu einer absoluten und selbst erhaltenden Oligarchie
wird. Sie muss Keinem Antwort stehen und ist niemals den Möglichkeiten
der Öffentlichkeit ausgesetzt das Personal auszutauschen oder "die
schwarzen Schafe auszusondern". Wenn keine Person oder Gruppe, seien es
Eigentümer oder Wähler, die regierende Elite austauschen kann, dann ist
diese Elite eher für eine Diktatur geeignet als für ein vorgeblich
demokratisches Land. Dennoch ist es bemerkenswert wieviele
selbsternannte Verfechter der "Demokratie", ob im In- oder Ausland, sich
für die vollkommene Unabhängigkeit der US-Notenbank aussprechen.

"Independent of politics" has a nice, neat ring to it, and has been
a staple of proposals for bureaucratic intervention and power ever since
the Progressive Era. Sweeping the streets; control of seaports;
regulation of industry; providing social security; these and many other
functions of government are held to be "too important" to be subject to
the vagaries of political whims. But it is one thing to say that
private, or market, activities should be free of government control, and
"independent of politics" in that sense. But these are *government*
agencies and operations we are talking about, and to say that
*government* should be "independent of politics" conveys very different
implications.  For government, unlike private industry on the market, is
not accountable either to stockholders or consumers. Government can only
be accountable to the public and to its representatives in the
legislature; and if government becomes "independent of politics" it can
only mean that that sphere of government becomes an absolute
self-perpetuating oligarchy, accountable to no one and never subject to
the public's ability to change its personnel or to "throw the rascals
out." If no person or group, whether stockholders or voters, can
displace a ruling elite, then such an elite becomes more suitable for a
dictatorship than for an allegedly democratic country.  And yet it is
curious how many self-proclaimed champions of "democracy," whether
domestic or global, rush to defend the alleged ideal of the total
independence of the Federal Reserve.


Abgeordneter des US-Kongresses Barney Frank (Demokrat aus 
Massachusetts), einer der Unterstützer des Gesetzesentwurfs von Gonzalez,
weist darauf hin, "wenn man die Prinzipien von denen heutzutage geredet
wird ernst nimmt," wie z.B. "die Reform der Regierung und größere
Transparenz der Regierung—werden sie vor allen Anderen von der
US-Notenbank missachtet". Auf welcher Grundlage sollte also dann das
"Prinzip" einer unabhängigen Notenbank fortbestehen?

Representative Barney Frank (D., Mass.), a co-sponsor of the
Gonzalez bill, points out that "if you take the principles that people
are talking about nowadays," such as "reforming government and opening
up government—the Fed violates it more than any other branch of
government." On what basis, then, should the vaunted "principle" of an
independent Fed be maintained?


Es ist sehr aufschlussreich zu untersuchen wer die Verteidiger dieses
vorgeblichen Prinzips sein könnten und welche Strategie sie verfolgen.
Vermutlich ist eines der politischen Behörden von dem die Fed
insbesondere unabhängig sein möchte das Finanzministerium der
Vereinigten Staaten. Und dennoch sagt Frank Newman, Minister im
Finanzministerium unter Präsident Clinton, bei seiner Ablehnung des
Entwurfs von Gonzalez: "Die Fed ist unabhängig und das ist eines seiner
grundlegenden Prinzipien". Einen weiteren Einblick bekommen wir durch
eine Bemerkung in der New York Times, die anmerkt welche Reaktion die
US-Notenbank auf den Gesetzesentwurf von Gonzalez hat: "Die Fed arbeitet
im Hintergrund schon daran Heerscharen von Banker zu organisieren, die
gegen eine Politisierung der Zentralbank aufheulen sollen" (New York
Times, 12 Oktober 1993). Warum sollten aber diese "Heerscharen von
Banker" sich derart eifrig und bereitwillig mobilisieren lassen, wenn es
doch um die Verteidigung einer Regierungsbehörde geht, deren Kontrolle
und Regulierung sie unterliegen und von der viele Tätigkeiten im
Bankensystem bestimmt werden? Liegt irgend eine Art Kontrolle, ein
Gegengewicht für ihren Herr und Meister, nicht in ihrem eigenen
Interesse? Warum sollte sich eine regulierte und kontrollierte Industrie
derart in die uneingeschränkte Macht ihres eigenen Aufsehers verliebt
haben?

It is instructive to examine who the defenders of this alleged
principle may be, and the tactics they are using.  Presumably one
political agency the Fed particularly wants to be independent from is
the U.S. Treasury. And yet Frank Newman, President Clinton's Under
Secretary of the Treasury for Domestic Finance, in rejecting the
Gonzalez reform, states: 'The Fed is independent and that's one of the
underlying concepts." In addition, a revealing little point is made by
the New York Times, in noting the Fed's reaction to the Gonzalez bill:
'The Fed is already working behind the scenes to organize battalions of
bankers to howl about efforts to politicize the central bank" (New York
Times, October 12, 1993). True enough. But why should these "battalions
of bankers" be so eager and willing to mobilize in behalf of the Fed's
absolute control of the monetary and banking system?  Why should bankers
be so ready to defend a federal agency which controls and regulates
them, and virtually determines the operations of the banking system?
Shouldn't private banks want to have some sort of check, some curb, upon
their lord and master? Why should a regulated and controlled industry be
so much in love with the unchecked power of their own federal
controller?


Betrachten wir doch mal die Situation einer beliebigen anderen
Industrie. Wäre es nicht etwas verdächtig, wenn bspw. die
Versicherungsgesellschaften eine uneingeschränkte Macht für ihre
Regulierungsbehörde fordern würde, oder die Transportindustrie für ihre
Regulierungsbehörde, oder wenn Pharmaunternehmen um totale und geheime
Macht für ihre Zulassungbehörde ringen würden? Sollten wir nicht dem
merkwürdig kuscheligen Verhältnis zwischen den Banken und der
US-Notenbank äußerst skeptisch gegenüberstehen? Was geht hier vor sich?
Unsere Aufgabe in diesem Buch ist es die US-Notenbank der Untersuchung
auszusetzen, die sie in der Öffentlichkeit leider nicht bekommt.

Let us consider any other private industry. Wouldn't it be just a
tad suspicious if, say, the insurance industry demanded unchecked power
for their state regulators, or the trucking industry total power for the
ICC, or the drug companies were clamoring for total and secret power to
the Food and Drug Administration? So shouldn't we be very suspicious of
the oddly cozy relationship between the banks and the Federal Reserve?
What's going on here? Our task in this volume is to open up the Fed to
the scrutiny it is unfortunately not getting in the public arena.


Absolute Macht und keinerlei Prüfung der US-Notenbank werden meistens
alleine auf einer Grundlage verteidigt: Jede Änderung würde die
angeblich unerbittliche Hingabe der Fed einschränken, den scheinbar
unendlichen "Kampf gegen die Inflation" zu führen. Das ist das immer
gleiche Lied in der Verteidigung der uneingeschränkten Macht der
US-Notenbank. Die Reformen von Gonzalez, so warnen die Beamten der
Fed, könnten vom Finanzmarkt "als Schwächung der Fähigkeit der Fed
gegen Inflation anzukämpfen" aufgefasst werden (New York Times, 8
Oktober 1993). Alan Greenspan, der Vorsitzende der Fed, ging in darauf
folgende Anhörungen im Kongress näher auf diesen Punkt ein. Politiker,
und vermeintlich auch die Öffentlichkeit, sind immer in der Versuchung
die Geldmenge auszuweiten und damit (Preis-) Inflation zu verschlimmern.
Nach der Meinung von Greenspan gibt es:

Absolute power and lack of accountability by the Fed are generally
defended on one ground alone: that any change would weaken the Federal
Reserve's allegedly inflexible commitment to wage a seemingly permanent
"fight against inflation." This is the Johnny-one-note of the Fed's
defense of its unbridled power. The Gonzalez reforms, Fed officials
warn, might be seen by financial markets "as weakening the Fed's ability
to fight inflation" (New York Times, October 8, 1993). In subsequent
Congressional testimony, Chairman Alan Greenspan elaborated this point.
Politicians, and presumably the public, are eternally tempted to expand
the money supply and thereby aggravate (price) inflation. Thus to
Greenspan:


> Die Versuchung aufs monetäre Gaspedal zu treten oder zumindest die
> monetäre Bremse bis nach der nächsten Wahl zu vermeiden... Sich solche
> Versuchungen hinzugeben wird der Wirtschaft eine inflationäre Neigung
> verleihen, könnte sie unstabil machen, zu einer Rezession und
> wirtschaftlichem Abschwung führen.

> The temptation is to step on the monetary accelerator or at least to
> avoid the monetary brake until after the next election… Giving in to
> such temptations is likely to impart an inflationary bias to the 
> economy and could lead to instability, recession, and economic 
> stagnation.


Auf eine Prüfung der Fed zu verzichten, so Greenspan, ist ein kleiner
Preis um zu vermeiden, dass "die Währungspolitik dem Einfluss von
kurzsichtigen Politikern ausgesetzt wird, die immer wieder unter den
Druck von Wahlen stehen" (New York Times, 14 Oktober 1993).

The Fed's lack of accountability, Greenspan added, is a
small price to pay to avoid "putting the conduct of monetary
policy under the close influence of politicians subject to
short-term election cycle pressure" (New York Times, October
14,1993).


Da haben wir es also. Wenn man den Sagen der US-Notenbank glauben mag,
ist die Öffentlichkeit ein Ungeheuer mit einer unstillbaren Begierde für
Inflation und seine schrecklichen Folgen. Durch die gefürchteten und all
zu häufige Unannehmlichkeiten namens "Wahlen", sind Politiker dieser
Versuchung ausgesetzt, insbesondere solche Institutionen die besonders
häufig gewählt werden, wie das US Repräsentantenhaus, dass alle zwei
Jahre gewählt wird, und sich deswegen um so mehr dem Willen der
Öffentlichkeit beugt. Die US-Notenbank wird im Gegensatz dazu von
Experten für Währungspolitik geleitet, die von dem Verlangen der
Öffentlichkeit nach Inflation unabhängig sind. Sie stehen deshalb
jederzeit bereit, sich für die langfristigen Interessen der
Öffentlichkeit einzusetzen, und sich für den ewigen Kampf gegen das
Schreckgespenst der Inflation zu wappnen. Kurz gesagt, es ist
unerlässlich die Kontrolle über die Währung der US-Notenbank zu
überlassen, um die Öffentlichkeit vor sich selbst sowie seinen
kurzsichtigen Begierden und Versuchungen zu schützen. Ein Ökonom für
Währungspolitik, der zwischen 1920 bis in die 30er Jahre damit
verbrachte Zentralbanken in der dritten Welt aufzubauen wurde häufig
als "der Geld Doktor" bezeichnet. In unserer heutigen Zeit der
Therapien möchten Greenspan und seine Berater wohl als
"Währungtherapeuten" gesehen werden; gutmütige aber strenge Lehrmeister
denen wir totale Macht übertragen um uns vor uns selbst zu schützen.

So there we have it. The public, in the mythology of the Fed and its
supporters, is a great beast, continually subject to a lust for
inflating the money supply and therefore for subjecting the economy to
inflation and its dire consequences. Those dreaded all-too-frequent
inconveniences called "elections" subject politicians to these
temptations, especially in political institutions such as the House of
Representatives who come before the public every two years and are
therefore particularly responsive to the public will. The Federal
Reserve, on the other hand, guided by monetary experts independent of
the public's lust for inflation, stands ready at all times to promote
the long-run public interest by manning the battlements in an eternal
fight against the Gorgon of inflation. The public, in short, is in
desperate need of absolute control of money by the Federal Reserve to
save it from itself and its short-term lusts and temptations. One
monetary economist, who spent much of the 1920s and 1930s setting up
Central Banks throughout the Third World, was commonly referred to as
"the money doctor." In our current therapeutic age, perhaps Greenspan
and his confreres would like to be considered as monetary "therapists,"
kindly but stern taskmasters whom we invest with total power to save us
from ourselves.


Welchen Teil haben die Banker an dieser therapeutischen Behandlung?
Wenn man den Angestellten der Fed glauben mag, einen ganz wesentlichen.
Der Vorschlag von Gonzalez, die regionalen Präsidenten durch den
Präsident der Vereinigten Staaten bestimmen zu lassen, anstatt durch die
Banker der Region, würde wenn man den angestellten glauben mag, "es
der Zentralbank schwerer machen gegen Inflation anzukämpfen". Warum?
Weil die "Minimierung von Inflation" dadurch "am besten" erreicht wird,
"dass private Banker die regionalen Präsidenten bestimmen". Und warum
eignen sich privaten Banker dazu "am besten"? Den Angestellten der
Zentralbank nach, da private Banker "mitunter die Besten der Welt
sind, wenn es darum geht auf Inflation zu achten" (New York Times, 12
Oktober 1993).

But in this administering of therapy, where do the private bankers
fit in? Very neatly, according to Federal Reserve officials. The
Gonzalez proposal to have the president instead of regional bankers
appoint regional Fed presidents would, in the eyes of those officials,
"make it harder for the Fed to clamp down on inflation/' Why? Because,
the "sure way" to "minimize inflation" is "to have private bankers
appoint the regional bank presidents." And why is this private banker
role such a "sure way"? Because, according to the Fed officials, private
bankers "are among the world's fiercest inflation hawks" (New York
Times, October 12,1993).


Damit wäre wie Weltanschauung der Zentralbank und seine Befürworter 
komplett. Die Öffentlichkeit und Politiker, die ja immer der Versuchung
ausgesetzt sind Geld zu drucken, akzeptieren nicht nur dieses Argument,
sondern auch den wesentlichen Beitrag der privaten Banker. Diese sind
"die Besten der Welt um auf Inflation zu achten" und können der Fed nur
eine Unterstützung sein bei ihrer ewigen Hingabe im Kampf gegen die
Inflation.

The worldview of the Federal Reserve and its advocates is now
complete. Not only are the public and politicians responsive to it
eternally subject to the temptation to inflate; but it is important for
the Fed to have a cozy partnership with private bankers. Private
bankers, as "the world's fiercest inflation hawks," can only bolster the
Fed's eternal devotion to battling against inflation.


So lautet die Ideologie der US-Notenbank entsprechend seiner eigenen
Propaganda, die durch respektable und etablierte Medien wie die New York
Times sowie in Erklärungen und Lehrbüchern unzähliger Ökonomen verbreitet
wird. Sogar Ökonomen die sich für Inflation aussprechen, akzeptieren und
verbreiten die Darstellung der Zentralbank nach ihrer eigenen
Vorstellung. Tatsächlich aber, ist alles an dieser Mythologie das genaue
Gegenteil der Wahrheit. Solange wir die betrügerische Legende von der
Zentralbank nicht aufgedeckt und zerstört haben, werden wir auch nicht
klar über Geld und das Finanzwesen denken können.

There we have the ideology of the Fed as reflected in its own
propaganda, as well as respected Establishment transmission belts such
as the New York Times, and in pronouncements and textbooks by countless
economists. Even those economists who would like to see more inflation
accept and repeat the Fed's image of its own role. And yet every aspect
of this mythology is the very reverse of the truth. We cannot think
straight about money, banking, or the Federal Reserve until this
fraudulent legend has been exposed and demolished.


An dieser weit verbreiteten Legende stimmt allerdings eines (und nur
eines: Die entscheidende Ursache für chronisch inflationäre Preise ist
die Inflation bzw. die Ausweitung der Menge an Geld im Umlauf. Ebenso
wie eine Erhöhung der Produktion oder des Angebots von Baumwolle für
einen geringeren Preis auf dem Markt sorgt, so wird durch die Schaffung
von neuem Geld auch jeder Euro oder Doller etwas billiger und verliert
an Kaufkraft.

There is, however, one and only one aspect of the common legend that
is indeed correct: that the overwhelmingly dominant cause of the virus
of chronic price inflation is inflation, or expansion, of the supply of
money. Just as an increase in the production or supply of cotton will
cause that crop to be cheaper on the market; so will the creation of
more money make its unit of money, each franc or dollar, cheaper and
worth less in purchasing power of goods on the market.


Lasst uns aber nun diese Tatsache vor dem Hintergrund einer
Zentralbank betrachten. Wir sind vorgeblich mit einem Verlangen der
Öffentlichkeit nach Inflation konfrontiert, währen die Zentralbank und
seine Komplizen sich entschlossen gegen diese Kurzsichtigkeit stemmen.
Wie soll aber die Öffentlichkeit diese Inflation herbeiführen? Wie kann
die Öffentlichkeit mehr Geld erschaffen bzw. drucken? Das zu machen wäre
schwierig, denn es gibt nur eine Institution in unserer Gesellschaft mit
dieser Befugnis. Wer versucht Geld zu drucken macht sich der schweren
Straftat der "Geldfälschung" schuldig, die von der Regierung sehr ernst
genommen wird. Während die Regierung sich relativ gutmütig gegenüber
Vergehen und Verbrechen, wie z.B. Überfälle, Diebstahl und Mord verhält, 
und sich um "soziale Benachteiligung" bei jugendlichen Kriminellen
sorgt, gibt es eine Gruppe von Kriminellen die von der Regierung niemals
mit Geduld behandelt wird; denn sie begehen ein Verbrechen, dass von der
Regierung sehr ernst genommen wird; sie bereichern sich an der
Einkommensquelle der Regierung: Namentlich an dem Monopol über das
Drucken von Geld an der sich die Zentralbank erfreut.

But let us consider this agreed-upon fact in the light of the above
myth about the Federal Reserve. We supposedly have the public clamoring
for inflation while the Federal Reserve, flanked by its allies the
nation's bankers, resolutely sets its face against this short-sighted
public clamor. But how is the public supposed to go about achieving this
inflation?  How can the public create, i.e., "print/7 more money? It
would be difficult to do so, since only one institution in the society
is legally allowed to print money. Anyone who tries to print money is
engaged in the high crime of "counterfeiting," which the federal
government takes very seriously indeed. Whereas the government may take
a benign view of all other torts and crimes, including mugging, robbery,
and murder, and it may worry about the "deprived youth" of the criminal
and treat him tenderly, there is one group of criminals whom no
government ever coddles: the counterfeiters.  The counterfeiter is
hunted down seriously and efficiently, and he is salted away for a very
long time; for he is committing a crime that the government takes very
seriously: he is interfering with the government's revenue:
specifically, the monopoly power to print money enjoyed by the Federal
Reserve.


"Geld" besteht in unserer Wirtschaft aus Papier und wird von der
Zentralbank herausgegeben. Auf ihren Scheinen steht gedruckt: "Diese
Banknote ist legales Zahlungsmittel für alle Schulden ob privat oder
öffentlich". Diese "Scheine der US-Notenbank" sind Geld und sonst
nichts; sie müssen von jedem Händler oder Gläubiger angenommen werden,
ob sie es wollen oder nicht.

"Money," in our economy, is pieces of paper issued by the Federal
Reserve, on which are engraved the following: "This Note is Legal Tender
for all Debts, Private, and Public." This "Federal Reserve Note," and
nothing else, is money, and all vendors and creditors must accept these
notes, like it or not.


Wenn die chronische Inflation in Amerika und fast jedem anderen Land
durch die ständige Erschaffung von neuem Geld verursacht wird, *und
wenn* in jedem Land die Zentralbank der Regierung das Monopol auf die
Geldschöpfung hat und die Quelle allen Geldes ist, *wer ist dann wohl*
für die Pest der Inflation verantwortlich? Wer sonst, außer eben diese
Institution die als einzige ermächtigt ist Geld zu erschaffen, also die
Zentralbank *selbst*?

So: if the chronic inflation undergone by Americans, and in almost
every other country, is caused by the continuing creation of new money,
*and if* in each country its governmental "Central Bank" (in the United
States, the Federal Reserve) is the sole monopoly source and creator of
all money, *who then* is responsible for the blight of inflation? Who
except the very institution that is solely empowered to create money,
that is, the Fed (and the Bank of England, and the Bank of Italy, and
other central banks) *itself*?


Kurz gesagt: Selbst wenn wir das Problem noch nicht im Detail untersucht
haben, sollte ein Schimmer der Wahrheit schon zu erkennen sein: nämlich,
dass der Propagandafeldzug wonach die Fed ein Bollwerk gegen die durch
Andere herbeigeführte Inflation errichtet hat, nichts anderes ist als
eine Täuschung. Der alleinige Verantwortliche für die Inflation, die
US-Notenbank, klagt ständig über Inflation wofür scheinbar *alle
anderen* in der Gesellschaft verantwortlich sind. Es ist der alte Trick
eines Räubers der aufschreit "Haltet den Dieb!" während er die Straße
runterrennt und auf andere deutet. Wir sehen warum es für die Fed und
alle anderen Zentralbanken immer wichtig gewesen ist sich mit einer
geheimnisvollen Aura der Ernsthaftigkeit zu schmücken. Denn, wie wir
noch klarer sehen werden, wenn die Öffentlichkeit sich darüber im klaren
wäre was sich abspielt, wenn es den Vorhang der unergründlichen Zauberer
aufreißen würden, könnte sie bald entdecken, dass die US-Notenbank die
Ursache des Problems ist. Was wir brauchen ist nicht eine vollkommen
unabhängige und allmächtige Zentralbank, sondern gar keine Zentralbank.

In short: even before examining the problem in detail, we should
already get a glimmer of the truth: that the drumfire of propaganda that
the Fed is manning the ramparts against the menace of inflation brought
about by others is nothing less than a deceptive shell game. The culprit
solely responsible for inflation, the Federal Reserve, is continually
engaged in raising a hue-and-cry about "inflation," for which virtually
*everyone else* in society seems to be responsible.  What we are seeing is
the old ploy by the robber who starts shouting "Stop, thief!" and runs
down the street pointing ahead at others. We begin to see why it has
always been important for the Fed, and for other Central Banks, to
invest themselves with an aura of solemnity and mystery For, as we shall
see more fully, if the public knew what was going on, if it was able to
rip open the curtain covering the inscrutable Wizard of Oz, it would
soon discover that the Fed, far from being the indispensable solution to
the problem of inflation, is itself the heart and cause of the problem.
What we need is not a totally independent, all-powerful Fed; what we
need is no Fed at all.

