Creative Commons - Namensnennung 3.0
http://creativecommons.org/licenses/by/3.0/de/


Sie dürfen:

  - das Werk bzw. den Inhalt vervielfältigen, verbreiten und öffentlich
    zugänglich machen.
  - Abwandlungen und Bearbeitungen des Werkes bzw. Inhaltes anfertigen.

Zu den folgenden Bedingungen:
    
  - **Namensnennung** — Sie müssen den Namen des Autors/Rechteinhabers in
    der von ihm festgelegten Weise nennen.

Wobei gilt:

  - **Verzichtserklärung** — Jede der vorgenannten Bedingungen kann
    aufgehoben werden, sofern Sie die ausdrückliche Einwilligung des
    Rechteinhabers dazu erhalten.

  - **Public Domain (gemeinfreie oder nicht-schützbare Inhalte)** —
    Soweit das Werk, der Inhalt oder irgendein Teil davon zur Public
    Domain der jeweiligen Rechtsordnung gehört, wird dieser Status von
    der Lizenz in keiner Weise berührt.

  - **Sonstige Rechte** — Die Lizenz hat keinerlei Einfluss auf die
    folgenden Rechte:

    - Your fair dealing or fair use rights, or other applicable copyright
      exceptions and limitations;

    - Das Urheberpersönlichkeitsrecht des Rechteinhabers;
    - Rechte anderer Personen, entweder am Lizenzgegenstand selber oder
      bezüglich seiner Verwendung, zum Beispiel Persönlichkeitsrechte
      abgebildeter Personen.

**Hinweis** — Im Falle einer Verbreitung müssen Sie anderen alle
Lizenzbedingungen mitteilen, die für dieses Werk gelten. Am einfachsten
ist es, an entsprechender Stelle einen Link auf diese Seite einzubinden.


Vollständige Lizenz:
http://creativecommons.org/licenses/by/3.0/de/legalcode
